/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */

package org.immunization.record.api.impl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public interface NotifyService {
    public void addListener(ActionListener listener);
    /*public void removeListener(ActionListener listener);*/
    public void notifyListeners(ActionEvent action);
    public void notify(String serviceType, ActionEvent action);
}
