/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.record.api.impl;

import java.time.LocalDate;
import java.time.Period;
import org.immunization.record.api.RecommendationShot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultRecommendationShot extends DefaultShot implements RecommendationShot{

    private Period shotDatePeriod;

    private LocalDate minimumShotDate;
    private Period minimumShotDatePeriod;
    private String comment;
    
    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public void setShotDatePeriod(Period effectivePeriod) {
        shotDatePeriod = effectivePeriod;
    }

    @Override
    public Period getShotDatePeriod() {
        return shotDatePeriod;
    }

    public LocalDate getMinimumShotDate() {
        return minimumShotDate;
    }

    public void setMinimumShotDate(LocalDate minimumShotDate) {
        this.minimumShotDate = minimumShotDate;
    }

    public Period getMinimumShotDatePeriod() {
        return minimumShotDatePeriod;
    }

    public void setMinimumShotDatePeriod(Period minimumShotDatePeriod) {
        this.minimumShotDatePeriod = minimumShotDatePeriod;
    }
}
