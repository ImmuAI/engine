/*
 *
 * SPDX-FileCopyrightText: 2020-2022 Weimim Wu <wu.weimin.01@gmail.com>
 * SPDX-FileCopyrightText: 2020-2022 Casita Data Processing LLC
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package casitadp.immunization.resource.persistence;

import casitadp.immunization.process.graph.DefaultScheduleWeightedGraph;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.immunization.process.api.impl.DefaultResourceService;
import org.immunization.process.api.impl.DefaultScheduleService;
import org.immunization.process.api.impl.DefaultVaccineService;
import org.immunization.record.api.Vaccine;
import org.immunization.record.api.impl.DefaultVaccine;
import org.immunization.schedule.api.ConstraintOf;
import org.immunization.schedule.api.Dose;
import org.immunization.schedule.api.VaccineComponent;
import org.immunization.schedule.api.impl.AbstractConstraint;
import org.immunization.schedule.api.impl.DefaultDose;
import org.immunization.schedule.api.impl.DefaultVaccineComponent;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class Resource {

    private final static String DEFAULT_SCHEDULE = "jsonSchedule.json";
    private final static String VACCINE_NODE = "Vaccine";
    private final static String VACCINE_COMPONENT_NODE = "VaccineComponent";
    private final static String VACCINE_COMPONENT_MAPPING_NODE = "Connection";
    private final static String SERIES_NODE = "Series";

    private static final Logger logger = Logger.getLogger(
            Resource.class.getPackage().getName());

    public static Resource loadResource() throws FileNotFoundException, IOException {

        InputStream input = getInputStream(DEFAULT_SCHEDULE);
        if (input != null) {
            try {
                Resource v = loadResource(input);
                return v; //add to centrallookup
            } finally {
                if (input != null) {
                    input.close();
                }
            }
        }
        return null;
    }

    public static InputStream getInputStream(String resource) throws FileNotFoundException {
        if (Resource.class.getClass().getResource(resource) != null) {
            try {
                return Resource.class.getClass().getResource(resource).openStream();
            } catch (IOException ex) {
                logger.log(Level.WARNING, "No resource found for {0}", resource);
            }
        }
        InputStream is = Resource.class.getClassLoader().getResourceAsStream(resource);

        return is;
    }

    public static Resource loadResource(InputStream input) throws IOException {
        Resource vs = null;

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        // read json data
        JsonNode jsonMap = mapper.readTree(input);
        ObjectReader reader;
//read vaccins
        JsonNode vaccineArray = jsonMap.get(VACCINE_NODE);
        reader = mapper.readerFor(new TypeReference<List<DefaultVaccine>>() {
        });
        List<DefaultVaccine> vaccines
                = reader.readValue(vaccineArray);

//read vaccine components
        //     JsonNode componentArray = jsonMap.get(VACCINE_COMPONENT_NODE);
        //     reader = mapper.readerFor(new TypeReference<List<DefaultVaccineComponent>>() {
        //     });
        //     List<DefaultVaccineComponent> components
        //             = reader.readValue(componentArray);
//read vaccine mapping        
        JsonNode vcMappingArray = jsonMap.get(VACCINE_COMPONENT_MAPPING_NODE);
        reader = mapper.readerFor(new TypeReference<List<VaccineComponentMapping>>() {
        });
        List<VaccineComponentMapping> vcMapping
                = reader.readValue(vcMappingArray);

        //read resource series
        JsonNode seriesArray = jsonMap.get(SERIES_NODE);
        reader = mapper.readerFor(new TypeReference<List<SeriesInJson>>() {
        });
        List<SeriesInJson> series
                = reader.readValue(seriesArray);
        
        createScheduleService(series);
        createVaccineService(vaccines, series, vcMapping);
        return vs;
    }

    private static void createVaccineService(List<DefaultVaccine> vaccines,
            List<SeriesInJson> series,
            List<VaccineComponentMapping> vcMapping) {
        if (vaccines == null || vcMapping == null
                || series == null || series.isEmpty()
                || vaccines.isEmpty() || vcMapping.isEmpty()) {
            return;
        }
        /* store all VaccineComponent */
        ArrayList<DefaultVaccineComponent> components = new ArrayList<>();
        series.forEach(sj -> {
            for (DefaultVaccineComponent component : sj.getVaccineComponent()) {
                component.setSeriesCode(sj.getCode());
                components.add(component);
            }
        });
        
        /*build vaccine map */
        HashMap<String, Vaccine> vaccine = new HashMap<>();
        vaccines.forEach(v -> {
            vaccine.put(v.getCode(), v);
        });

        /*build vaccine component map */
        HashMap<String, DefaultVaccineComponent> vaccineComponent = new HashMap<>();
        components.forEach(component -> {
            vaccineComponent.put(component.getCode(), component);
        });
        /*build vaccine and its components map AND build component and its mapped vaccine map */
        HashMap<String, Collection<VaccineComponent>> vaccineMap = new HashMap<>();
        ArrayList<VaccineComponent> componentList;
        HashMap<String, Collection<Vaccine>> componentMap = new HashMap<>();
        ArrayList<Vaccine> vaccineList;
        for (VaccineComponentMapping vmap : vcMapping) {
            if (vaccineMap.get(vmap.getVaccine()) == null) {
                componentList = new ArrayList<>();
                vaccineMap.put(vmap.getVaccine(), componentList);
            }
            componentList = (ArrayList<VaccineComponent>) vaccineMap.get(vmap.getVaccine());
            componentList.add(vaccineComponent.get(vmap.getComponent()));

            if (componentMap.get(vmap.getComponent()) == null) {
                vaccineList = new ArrayList<>();
                componentMap.put(vmap.getComponent(), vaccineList);
            }
            vaccineList = (ArrayList<Vaccine>) componentMap.get(vmap.getComponent());
            vaccineList.add(vaccine.get(vmap.getVaccine()));
        }
        DefaultVaccineService service = new DefaultVaccineService();
        service.setVaccines(vaccineMap);
        service.setVaccineComponents(componentMap);

        /*add Vaccine Service to Resource Service*/
        DefaultResourceService rservice = (DefaultResourceService) DefaultResourceService.getInstance();
        rservice.setVaccineManager(service);
    }

    private static void createScheduleService(List<SeriesInJson> series) {
        if (series == null || series.isEmpty()) {
            return;
        }

        DefaultScheduleService service = new DefaultScheduleService();

        /* hold all doses */
        HashMap<String, Dose> tmp = new HashMap<>();
        /* Iterate all series.nodes */
        for (SeriesInJson sj : series) {
            if (sj.getDoses() == null || sj.getCode().isBlank()) {
                logger.log(Level.INFO, "Series not found - {0}", sj.getName());
                continue;
            }

            DefaultScheduleWeightedGraph sgraph = new DefaultScheduleWeightedGraph();
            // DefaultScheduleGraph sgraph = new DefaultScheduleGraph();
            /*add dose to series graph*/
            for (DoseInJson ds : sj.getDoses()) {
                Dose dose = createDose(ds);
                tmp.put(dose.getDoseID(), dose);
                sgraph.addNode(dose);
            }
            /*add connections of doses*/
            for (DoseConnection dc : sj.getDoseConnection()) {
                if (dc.getStart() == null || dc.getEnd() == null
                        || dc.getStart().isBlank() || dc.getEnd().isBlank()
                        || tmp.get(dc.getStart()) == null || tmp.get(dc.getEnd()) == null) {
                    logger.log(Level.SEVERE, "Null value not allowed  {0} ; {1}",
                            new Object[]{dc.getStart(), dc.getEnd()});

                }
                sgraph.addEdge(tmp.get(dc.getStart()), tmp.get(dc.getEnd()), (double) dc.getRanking());
                // sgraph.addEdge(tmp.get(dc.getStart()), tmp.get(dc.getEnd()));

            }
            service.setSeriesSchedule(sj.getCode(), sgraph);
            //--sgraph.getSuccessor();

        }
        /*add Schedule Service to Resource Service*/
        DefaultResourceService rservice = (DefaultResourceService) DefaultResourceService.getInstance();
        rservice.setScheduleManager(service);
    }

    private static Dose createDose(DoseInJson ds) {
        DefaultDose d = new DefaultDose();
        d.setDoseID(ds.getId());
        /*age*/
        d.addConstraint(new AbstractConstraint<>(ConstraintOf.VACCINE, ds.getVaccine()));
        d.addConstraint(new AbstractConstraint<>(ConstraintOf.AGE_DATE_RANGE, ds.getAgeRange()));
        d.addConstraint(new AbstractConstraint<>(ConstraintOf.MINIMUM_AGE_DATE_RANGE, ds.getMinAgeRange()));
        d.addConstraint(new AbstractConstraint<>(ConstraintOf.RECOMMEND_AGE_DATE_RANGE, ds.getRecommendedAgeRange()));
        /*Interval*/
        if (ds.getMinIntervalRange() != null) {
            d.addConstraint(new AbstractConstraint<>(ConstraintOf.MINIMUM_INTERVAL_RANGE, ds.getMinIntervalRange()));
        }
        if (ds.getRecommIntervalRange() != null) {
            d.addConstraint(new AbstractConstraint<>(ConstraintOf.RECOMMEND_INTERVAL_RANGE, ds.getRecommIntervalRange()));
        }
        if (ds.getMinIntervalRange1() != null) {
            d.addConstraint(new AbstractConstraint<>(ConstraintOf.MINIMUM_INTERVAL_TO_PRIOR2_RANGE, ds.getMinIntervalRange1()));
        }
        if (ds.getAccelerateIntervalRange() != null) {
            d.addConstraint(new AbstractConstraint<>(ConstraintOf.ACC_MINIMUM_INTERVAL_RANGE, ds.getAccelerateIntervalRange()));
        }
        /**/
        if (ds.getDobRange() != null) {
            d.addConstraint(new AbstractConstraint<>(ConstraintOf.DOB_DATE_RANGE, ds.getDobRange()));
        }
        if (ds.getEffectRange() != null) {
            d.addConstraint(new AbstractConstraint<>(ConstraintOf.LICENSE_DATE_RANGE, ds.getEffectRange()));
        }
        /**/
        if (ds.isFemaleOnly()) {
            d.addConstraint(new AbstractConstraint<>(ConstraintOf.FEMALE_ONLY, ds.isFemaleOnly()));
        }
        //d.addConstraint(new AbstractConstraint<>(ConstraintOf.MALE_ONLY, !ds.isFemaleOnly()));
        if (ds.isEvaluationOnly()) {
            d.addConstraint(new AbstractConstraint<>(ConstraintOf.EVALUATION_ONLY, ds.isEvaluationOnly()));
        }
        if (ds.isRiskGroup()) {
            d.addConstraint(new AbstractConstraint<>(ConstraintOf.INC_RISK_GROUP, ds.isRiskGroup()));
        }
        /**/
        if (ds.getOptDoseGivenBy() != null) {
            d.addConstraint(new AbstractConstraint<>(ConstraintOf.OPTIONAL_GIVEN_BY, ds.getOptDoseGivenBy()));
        }
        if (ds.getOptimalVaccine() != null) {
            d.addConstraint(new AbstractConstraint<>(ConstraintOf.USE_FIRST_VACCINE, ds.getOptimalVaccine()));
        }

        return d;
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Resource.loadResource();
        //     ScheduleService rs = loadRule();
        //     rs = null;
    }
}
