/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.api.impl;

import java.time.LocalDate;
import java.util.Collection;
import org.immunization.schedule.api.Gender;
import org.immunization.schedule.api.ScheduleCase;
import org.immunization.schedule.api.Shot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultScheduleCase implements ScheduleCase<LocalDate, Shot<LocalDate>> {

    private Collection<Shot<LocalDate>> shots;
    private ScheduleCase<LocalDate, org.immunization.record.api.Shot<LocalDate>> record;

    public DefaultScheduleCase(final ScheduleCase<LocalDate, 
            org.immunization.record.api.Shot<LocalDate>> record) {
        this.record = record;
    }

    @Override
    public LocalDate getDob() {
        return record.getDob();
    }

    @Override
    public void setDob(LocalDate dob) {
    }

    @Override
    public LocalDate getEvaluationDate() {
        return record.getEvaluationDate() == null? 
                LocalDate.now(): record.getEvaluationDate();
    }

    @Override
    public void setEvaluationDate(LocalDate evaluationDate) {
    }

    @Override
    public Gender getGender() {
        return record.getGender();
    }

    @Override
    public void setGender(Gender gender) {
    }

    @Override
    public Collection<Shot<LocalDate>> getShots() {
        return shots;
    }

    @Override
    public void setShots(final Collection<Shot<LocalDate>> shots) {
        this.shots = shots;
    }
}
