/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.util;

import java.time.LocalDate;
import java.time.Period;
import org.immunization.schedule.api.Constraint;
import org.immunization.schedule.api.ConstraintOf;
import org.immunization.schedule.api.DateRange;
import org.immunization.schedule.api.PeriodRange;
import org.immunization.schedule.api.impl.AbstractConstraint;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class AssertConstraint {

    public static boolean affirm(Constraint constraint, Object value) {
        ConstraintOf name = constraint.getName();
        switch (name) {
            case VACCINE:
                return assertStringConstraint((AbstractConstraint<String>) constraint, (String) value);
            case DOB_DATE_RANGE:
            case LICENSE_DATE_RANGE:
                return assertDateRangeConstraint((AbstractConstraint<DateRange<LocalDate>>) constraint, (LocalDate) value);
            case EVALUATION_ONLY:

        }
        return false;
    }

    public static boolean affirm(Constraint constraint, LocalDate base, LocalDate next) {
        ConstraintOf name = constraint.getName();
        switch (name) {
            case OPTIONAL_GIVEN_BY:
            case MINIMUM_AGE_DATE_RANGE:
                return assertPeriodRangeConstraint(
                        (AbstractConstraint<PeriodRange<Period>>) constraint,
                        base, next);
                
            case MINIMUM_INTERVAL_RANGE:
            case MINIMUM_INTERVAL_TO_PRIOR2_RANGE:
                return constraint.getConstraint() == null
                        ? true : assertPeriodRangeConstraint(
                                (AbstractConstraint<PeriodRange<Period>>) constraint,
                                base, next);

        }
        return false;
    }

    private static boolean assertStringConstraint(AbstractConstraint<String> constraint, String value) {
        return constraint.getConstraint().equals(value);
    }

    /* including from , not include to */
    private static boolean assertDateRangeConstraint(
            AbstractConstraint<DateRange<LocalDate>> constraint, LocalDate date) {
        return isDateInRange(date, constraint.getConstraint());
    }

    private static boolean assertPeriodRangeConstraint(
            AbstractConstraint<PeriodRange<Period>> constraint,
            LocalDate base, LocalDate next) {
        PeriodRange<Period> pr = constraint.getConstraint();
        LocalDate f = pr.getFrom() == null ? base : base.plus(pr.getFrom());
        LocalDate t = pr.getTo() == null ? next.plusYears(99L) : base.plus(pr.getTo());
        return isDateInRange(next, f, t);
    }

    /* if either from or to is null, then no restriction on its side 
    date is [from, to)
     */
    private static boolean isDateInRange(LocalDate date, DateRange<LocalDate> range) {
        return ((range.getFrom() == null || !range.getFrom().isAfter(date))
                && (range.getTo() == null || range.getTo().isAfter(date)));
    }
   /* [from, to)
    */
    private static boolean isDateInRange(LocalDate date, LocalDate from, LocalDate to) {
        return (!from.isAfter(date) && to.isAfter(date));
    }
    /* date is in [from, to]   */
  /*  private static boolean isDateInRangeIncludeTo(LocalDate date, LocalDate from, LocalDate to) {
        return (!from.isAfter(date) && !to.isBefore(date));
    }
*/
}
