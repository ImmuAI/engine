/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api;

import java.util.Collection;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public interface Dose {

    void addConstraint(Constraint c);

    Constraint getConstraint(ConstraintOf name);

    void removeConstraint(ConstraintOf name);

    Collection<Constraint> getAllConstraint();

    String getDoseID();

    void setDoseID(String id);
}
