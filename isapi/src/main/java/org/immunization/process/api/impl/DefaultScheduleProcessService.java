/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.api.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.immunization.process.api.RecommendationFilter;
import org.immunization.process.api.ScheduleProcessService;
import org.immunization.process.series.api.EvaluateAShot;
import org.immunization.process.series.api.ProcessReport;
import org.immunization.process.series.api.ScheduleAShot;
import org.immunization.process.series.api.impl.DefaultEvaluateAShot;
import org.immunization.process.series.api.impl.DefaultScheduleAShot;
import org.immunization.process.series.api.impl.DoseFilterByConstraint;
import org.immunization.process.series.api.impl.SeriesProcessor;
import org.immunization.process.series.flu.api.impl.SimpleFluSeriesProcessor;
import org.immunization.record.api.impl.DefaultFluSeason;
import org.immunization.record.api.impl.ImmunizationCase;
import org.immunization.record.api.impl.ImmunizationReport;
import org.immunization.schedule.api.Shot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultScheduleProcessService implements ScheduleProcessService {

    /* forward transform */
    private CaseTransform ftransform;

    /* default processor that processes each series */
    private SeriesProcessor sp;
    /* backward transform */
    private TransformRecommendedShot btransform;
    /* addtional processor that narrow down the recommendation shots*/
    private List<RecommendationFilter> filters;
    //flu sereis
    private String fluSeries = "1501";
    private SimpleFluSeriesProcessor flup;

    @Override
    public ImmunizationReport process(ImmunizationCase immucase) {
        assert immucase != null;
        /* transform to series cases, stored in process report*/
        Collection<ProcessReport<Shot<LocalDate>, LocalDate>> seriesCaseReports;
        try {
            seriesCaseReports = ftransform.transform(immucase);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

        for (ProcessReport<Shot<LocalDate>, LocalDate> pr : seriesCaseReports) {
            if (pr.getSeries().equals(fluSeries)) {
                flup.setReport(pr);
                flup.process();
            } else {
                sp.setReport(pr);
                sp.process();
            }
        }

        /* transform report and return */
        ImmunizationReport ireport = btransform.transform(seriesCaseReports);
        ireport.setRecord(immucase);

        if (filters != null && filters.size() > 0) {
            for (RecommendationFilter filter : filters) {
                filter.process(ireport, seriesCaseReports);
            }
        }
        return ireport;
    }

    public DefaultScheduleProcessService() {
        init();
    }

    private void init() {
        ftransform = new CaseTransform();
        sp = new SeriesProcessor();
        flup = new SimpleFluSeriesProcessor();
        flup.setFluSeason(new DefaultFluSeason());
        flup.setProcessor(sp);
        //
        EvaluateAShot evaluator = new DefaultEvaluateAShot();
        DoseFilterByConstraint filter = new DoseFilterByConstraint();
        evaluator.setDoseFilter(filter);
        sp.setEvaluateAShot(evaluator);
        ScheduleAShot scheduler = new DefaultScheduleAShot();
        sp.setScheduleAShot(scheduler);
        btransform = new TransformRecommendedShot();
        filters = new ArrayList<>();
        filters.add(new MatchVaccineHistoryProcessor());
    }

    protected Collection<ProcessReport<Shot<LocalDate>, LocalDate>>
            transform(ImmunizationCase immucase) throws Exception {
        return ftransform.transform(immucase);
    }

    protected SeriesProcessor getSp() {
        return sp;
    }

    protected ImmunizationReport transform(
            Collection<ProcessReport<Shot<LocalDate>, LocalDate>> seriesCaseReports) {
        ImmunizationReport rpt = btransform.transform(seriesCaseReports);
        return rpt;
    }

    protected ProcessReport<Shot<LocalDate>, LocalDate> process(ImmunizationCase immucase,
            String scode) {
        assert immucase != null && !scode.isBlank();
        /* transform to series cases, stored in process report*/
        Collection<ProcessReport<Shot<LocalDate>, LocalDate>> seriesCaseReports;
        try {
            seriesCaseReports = ftransform.transform(immucase);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

        for (ProcessReport<Shot<LocalDate>, LocalDate> pr : seriesCaseReports) {
            if (scode.equals(fluSeries)) {
                if (pr.getSeries().equals(fluSeries)) {
                    flup.setReport(pr);
                    flup.process();
                    return pr;
                }
            } else {
                if (pr.getSeries().equals(scode)) {
                    sp.setReport(pr);
                    sp.process();
                    return pr;
                }
            }
        }

        /* transform report and return */
        return null;
    }

    public List<RecommendationFilter> getFilters() {
        return filters;
    }

    public void setFilters(List<RecommendationFilter> filters) {
        this.filters = filters;
    }
}
