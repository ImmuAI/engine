/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.api.impl;

import org.immunization.process.api.ResourceService;
import org.immunization.process.api.ScheduleService;
import org.immunization.process.api.VaccineService;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultResourceService
        implements ResourceService <String, String>{

    private static ResourceService<String, String> instance;
    private VaccineService<String, String> vaccineService;
    private ScheduleService scheduleService;

    public static ResourceService<String, String> getInstance() {
        if (instance == null) {
            instance = new DefaultResourceService();
        }
        return instance;
    }

    /**
     *
     * @return
     */
    @Override
    public VaccineService<String, String> getVaccineManager() {
        return this.vaccineService;
    }

    public void setVaccineManager(VaccineService<String, String> service) {
        this.vaccineService = service;
    }

    @Override
    public ScheduleService getScheduleManager() {
        return this.scheduleService;
    }

    public void setScheduleManager(ScheduleService service) {
        this.scheduleService = service;
    }
}
