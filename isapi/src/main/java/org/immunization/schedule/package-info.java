/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */

/**
 * The interface and default implementation of an Immunization Schedule model. 
 * <p>
 *
 * @since 1.0
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Mar 19, 2021 5:31:17 PM
 */

package org.immunization.schedule;
