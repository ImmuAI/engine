/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.api.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.immunization.process.api.RecommendationFilter;
import org.immunization.process.series.api.ProcessReport;
import org.immunization.record.api.RecommendationShot;
import org.immunization.record.api.Vaccine;
import org.immunization.record.api.impl.DefaultShot;
import org.immunization.record.api.impl.ImmunizationReport;
import org.immunization.schedule.api.Shot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class MatchVaccineHistoryProcessor implements RecommendationFilter {

    public void process(ImmunizationReport ireport,
            Collection<ProcessReport<Shot<LocalDate>, LocalDate>> seriesCaseReports) {
        assert ireport != null && seriesCaseReports != null;
        matchVaccineHistory(ireport, seriesCaseReports);
    }

    private void matchVaccineHistory(ImmunizationReport ireport,
            Collection<ProcessReport<Shot<LocalDate>, LocalDate>> seriesCaseReports) {

        List<org.immunization.record.api.Shot> sh = collectionToList(ireport.getRecord().getShots());
        if (sh == null) {
            return;
        }
        for (ProcessReport<Shot<LocalDate>, LocalDate> rpt : seriesCaseReports) {
            /* if original shots given in a series, then process */
            if (ireport.getShots().containsKey(rpt.getSeries())) {
                Collection<RecommendationShot> shots
                        = ireport.getShots().get(rpt.getSeries());
                /* following two loops to find one shot match in a series */
                outloop:
                for (RecommendationShot rshot : shots) {
                    Vaccine v = rshot.getVaccine();
                    for (org.immunization.record.api.Shot oneShot : sh) {
                        if (oneShot.getVaccine().getCode().equals(v.getCode())) {
                            if (ireport.getRecommendationShots() == null) {
                                ireport.setRecommendationShots(
                                        new HashMap<String, RecommendationShot>());
                            }
                            ireport.getRecommendationShots().put(rpt.getSeries(), rshot);
                            break outloop;
                        }
                    }
                }
            }
        }
    }

    private List<org.immunization.record.api.Shot>
            collectionToList(Collection<? extends org.immunization.record.api.Shot> sh) {
        if (sh != null && !sh.isEmpty()) {
            List<org.immunization.record.api.Shot> l = new ArrayList<>();
            for (org.immunization.record.api.Shot s : sh) {
                l.add((DefaultShot) s);
            }
            //Collections.sort(l);
            Collections.reverse(l);
            return l;
        }
        return null;
    }
}
