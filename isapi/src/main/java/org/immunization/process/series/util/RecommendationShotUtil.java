/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.util;

import java.time.LocalDate;
import java.time.Period;
import org.immunization.process.series.api.impl.DefaultEvaluationReport;
import org.immunization.schedule.api.Constraint;
import org.immunization.schedule.api.ConstraintOf;
import org.immunization.schedule.api.DateRange;
import org.immunization.schedule.api.Dose;
import org.immunization.schedule.api.PeriodRange;
import org.immunization.schedule.api.ScheduleCase;
import org.immunization.schedule.api.Shot;
import org.immunization.schedule.api.impl.DefaultDateRange;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class RecommendationShotUtil {

    public static DateRange<LocalDate> createRecommendedDateRange(Dose ds,
            DefaultEvaluationReport er,
            ScheduleCase<LocalDate, Shot<LocalDate>> scase) {
        Constraint<PeriodRange<Period>> ra
                = (Constraint<PeriodRange<Period>>) ds.getConstraint(ConstraintOf.RECOMMEND_AGE_DATE_RANGE);
        Constraint<PeriodRange<Period>> ri
                = (Constraint<PeriodRange<Period>>) ds.getConstraint(ConstraintOf.RECOMMEND_INTERVAL_RANGE);

        DateRange<LocalDate> dr = createDateRange(ra, ri, er, scase);

        if (ds.getConstraint(ConstraintOf.MINIMUM_INTERVAL_TO_PRIOR2_RANGE) != null) {
            Constraint<PeriodRange<Period>> cmi2
                    = (Constraint<PeriodRange<Period>>) ds.getConstraint(ConstraintOf.MINIMUM_INTERVAL_TO_PRIOR2_RANGE);

            Period mi2 = cmi2.getConstraint().getFrom();
            LocalDate sdate = er.getShotBeforePreviousValidShot().getShotDate();
            LocalDate dmi2 = sdate.plus(mi2);
            if (dr.getFrom().isBefore(dmi2)) {
                dr.setFrom(dmi2);
            }
        }

        if (dr != null) {
            Constraint<DateRange<LocalDate>> c
                    = ds.getConstraint(ConstraintOf.LICENSE_DATE_RANGE);
            if (c != null) {
                dr = mergeDataRange(c, dr);
            }
        }
        return dr;
    }

    public static DateRange<LocalDate> createMinimumDateRange(Dose ds,
            DefaultEvaluationReport er,
            ScheduleCase<LocalDate, Shot<LocalDate>> scase) {
        Constraint<PeriodRange<Period>> ma
                = ds.getConstraint(ConstraintOf.MINIMUM_AGE_DATE_RANGE);
        Constraint<PeriodRange<Period>> mi
                = ds.getConstraint(ConstraintOf.MINIMUM_INTERVAL_RANGE);
        DateRange<LocalDate> mr = createDateRange(ma, mi == null ? null : mi, er, scase);
        if (mr != null) { //?
            /* any minimum interval to shot before last valid shot*/
            mi = ds.getConstraint(ConstraintOf.MINIMUM_INTERVAL_TO_PRIOR2_RANGE);
            if (mi != null) {
                LocalDate date = er.getShotBeforePreviousValidShot().getShotDate();
                LocalDate f = date.plus(mi.getConstraint().getFrom());
                if (f.isAfter(mr.getFrom())) {
                    mr.setFrom(f);
                }
            }
        }
        Constraint<DateRange<LocalDate>> c
                = ds.getConstraint(ConstraintOf.LICENSE_DATE_RANGE);
        if (mr != null && c != null) {
            mr = mergeDataRange(c, mr);
        }
        return mr;
    }

    public static DateRange<LocalDate> createDateRange(Constraint<PeriodRange<Period>> ra,
            Constraint<PeriodRange<Period>> ri,
            DefaultEvaluationReport er,
            ScheduleCase<LocalDate, Shot<LocalDate>> scase) {
        LocalDate dob = scase.getDob();
        LocalDate from = ra.getConstraint().getFrom() == null ? LocalDate.from(dob)
                : dob.plus(ra.getConstraint().getFrom());
        LocalDate to = ra.getConstraint().getTo() == null ? null : dob.plus(ra.getConstraint().getTo());

        LocalDate date;
        // if (to != null && ri != null) { //hard restriction
        if (ri != null) { //first dose
            date = er.getPreviousValidShot().getShotDate();
            LocalDate f = date.plus(ri.getConstraint().getFrom());
            from = f.isAfter(from) ? f : from;
            //ri.getConstraint().getTo() 
            LocalDate t = ri.getConstraint().getTo() == null ? null : dob.plus(ri.getConstraint().getTo());
            if (to == null && t != null) {
                to = t;
            }
            if (to != null && t != null) {
                to = t.isBefore(to) ? t : to;
            }
        }

        LocalDate ed = scase.getEvaluationDate();
        if (to != null && ed.isAfter(to)) {
            return null;
        }
        from = ed.isAfter(from) ? ed : from;
        return new DefaultDateRange(from, to);
    }

    public static DateRange<LocalDate> mergeDataRange(Constraint constraint, DateRange<LocalDate> dr) {
        DefaultDateRange ldr = (DefaultDateRange) constraint.getConstraint();
        if (dr != null && ldr != null) {
            return ldr.mergeRange(dr);
        }
        return dr;
    }
}
