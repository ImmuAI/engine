ImmuAI is a framework provides a generic model and procedures for processing immunization record of a client or patient, maintaining Immunization Schedule, such as ACIP or Pink Book of CDC, individual vaccine usage or schedule, such as vaccine usage of vaccine package sheet.

ISAPI is a library that provides programming interface and default implementation. It contains the API for accepting immunization history of a client, evaluating the history and recommending vaccine the client needs in the near future. It also supports customized rules for processing immunization history. 

Engine is a library that contains default Immunization Schedule, in a JSON file.A user can add, remove, or edit vaccine, dose, schedule information by editing the file. An application can use the engine library to load the schedule and find Vaccine and Dose information for processing immunization records.   

Demo provides a basic example of programming using the libraries. 



