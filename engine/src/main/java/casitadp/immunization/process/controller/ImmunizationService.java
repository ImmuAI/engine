/*
 *
 * SPDX-FileCopyrightText: 2020-2022 Weimim Wu <wu.weimin.01@gmail.com>
 * SPDX-FileCopyrightText: 2020-2022 Casita Data Processing LLC
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */

package casitadp.immunization.process.controller;

import casitadp.immunization.resource.persistence.Resource;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.immunization.process.api.ScheduleService;
import org.immunization.process.api.VaccineService;
import org.immunization.process.api.impl.DefaultResourceService;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class ImmunizationService {

    public ImmunizationService() {
        init();
    }

    private void init() {
        try { //or ServiceProvider?
            Resource.loadResource();
        } catch (IOException ex) {
            Logger.getLogger(ImmunizationService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        new ImmunizationService();
        DefaultResourceService rservice = (DefaultResourceService) DefaultResourceService.getInstance();
        ScheduleService sservice =rservice.getScheduleManager(); 
        VaccineService vservice = rservice.getVaccineManager();
        sservice.getSeriesSchedule("1301").getSuccessor();
        vservice.getVaccineComponent(null);
    }
}
