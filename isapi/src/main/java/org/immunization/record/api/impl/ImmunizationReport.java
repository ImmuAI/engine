/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.record.api.impl;

import java.util.Collection;
import java.util.Map;
import org.immunization.record.api.RecommendationShot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class ImmunizationReport {

    private ImmunizationCase record;
    private Map<String, Collection<RecommendationShot>> shots;
    private Collection<RecommendationShot> combinedVaccineShots;
    private Collection<String> invalidShotInformation;
    private Collection<String> completedSeries;
    private Map<String, RecommendationShot> recommendationShots;
    private Collection<String> combinedVaccineShotSeries;

    public Collection<String> getCombinedVaccineShotSeries() {
        return combinedVaccineShotSeries;
    }

    public void setCombinedVaccineShotSeries(Collection<String> combinedVaccineShotSeries) {
        this.combinedVaccineShotSeries = combinedVaccineShotSeries;
    }
    
    public ImmunizationCase getRecord() {
        return record;
    }

    public void setRecord(ImmunizationCase record) {
        this.record = record;
    }

    public Map<String, Collection<RecommendationShot>> getShots() {
        return shots;
    }

    public void setShots(Map<String, Collection<RecommendationShot>> shots) {
        this.shots = shots;
    }

    public Collection<RecommendationShot> getCombinedVaccineShots() {
        return combinedVaccineShots;
    }

    public void setCombinedVaccineShots(Collection<RecommendationShot> combinedVaccineShots) {
        this.combinedVaccineShots = combinedVaccineShots;
    }

    public Collection<String> getInvalidShotInformation() {
        return invalidShotInformation;
    }

    public void setInvalidShotInformation(Collection<String> invalidShotInformation) {
        this.invalidShotInformation = invalidShotInformation;
    }

    public Collection<String> getCompletedSeries() {
        return completedSeries;
    }

    public void setCompletedSeries(Collection<String> completedSeries) {
        this.completedSeries = completedSeries;
    }

    public Map<String, RecommendationShot> getRecommendationShots() {
        return recommendationShots;
    }

    public void setRecommendationShots(Map<String, RecommendationShot> recommendationShots) {
        this.recommendationShots = recommendationShots;
    }
}
