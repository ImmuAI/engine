/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api.impl;

import java.time.LocalDate;
import org.immunization.schedule.api.DateRange;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultDateRange implements DateRange<LocalDate> {

    private LocalDate from = null;
    private LocalDate to = null;

    public DefaultDateRange() {
    }

    public DefaultDateRange(LocalDate from, LocalDate to) {
        if (from == null && to == null) {
            throw new NullPointerException("Null value found for data range.");
        }
        this.from = from;
        this.to = to;
    }
    
    public DefaultDateRange(DefaultDateRange dr) {
        this(dr.getFrom(), dr.getTo());
    }

    @Override
    public LocalDate getFrom() {
        return from;
    }

    @Override
    public void setFrom(LocalDate from) {
        if (from == null && to == null) {
            throw new NullPointerException("Null value found for data range.");
        }
        if (from != null && to != null && from.isAfter(to)) {
            throw new IllegalArgumentException("Date to should be after Date from.");
        }
        this.from = from;
    }

    public LocalDate getDefaultTo() {
        return to;
    }

    public LocalDate getDefaultFrom() {
        return from;
    }

    @Override
    public LocalDate getTo() {
        return to;
    }

    @Override
    public void setTo(LocalDate to) {
        if (from == null && to == null) {
            throw new NullPointerException("Null value found of DateRange.");
        }
        this.to = to;
    }

    public DateRange<LocalDate> mergeRange(DateRange<LocalDate> daterange) {
        if (daterange == null) {
            throw new NullPointerException("Null value found in Merging DateRange.");
        }

        if (from == null && daterange.getFrom() == null) {
            LocalDate t = daterange.getTo();
            t = t.isBefore(to) ? t : to;
            return new DefaultDateRange(null, t);
        }
        if (to == null && daterange.getTo() == null) {
            LocalDate f = daterange.getFrom();
            f = f.isAfter(from) ? f : from;
            return new DefaultDateRange(f, null);
        }
        if (from != null) {
            return mergeRange(this, daterange);
        } else {
            return mergeRange(daterange, this);
        }
    }

    private static DateRange<LocalDate> mergeRange(
            DateRange<LocalDate> dr0, DateRange<LocalDate> dr1) {
        LocalDate f = dr0.getFrom();
        LocalDate t = null;
        if (dr0.getTo() != null && dr1.getTo() == null) {
            if (dr1.getFrom().isAfter(dr0.getTo())) {
                return null;
            } else {
                f = f.isAfter(dr1.getFrom()) ? f : dr1.getFrom();
                t = dr0.getTo();
            }
        } else if (dr0.getTo() == null && dr1.getTo() != null) {
            if (f.isAfter(dr1.getTo())) {
                return null;
            } else {
                if (dr1.getFrom() != null) {
                    f = f.isAfter(dr1.getFrom()) ? f : dr1.getFrom();
                }
                t = dr1.getTo();
            }
        } else {
            if (f.isAfter(dr1.getTo())
                    || (dr1.getFrom() != null && dr1.getFrom().isAfter(dr0.getTo()))) {
                return null;
            } else {
                if (dr1.getFrom() != null) {
                    f = f.isAfter(dr1.getFrom()) ? f : dr1.getFrom();
                }
                t = dr0.getTo().isBefore(dr1.getTo())
                        ? dr0.getTo() : dr1.getTo();
            }
        }

        return new DefaultDateRange(f, t);
    }
}
