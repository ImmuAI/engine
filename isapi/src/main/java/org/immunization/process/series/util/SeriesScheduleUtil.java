/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.util;

import java.util.ArrayList;
import java.util.Collection;
import org.immunization.process.api.impl.DefaultResourceService;
import org.immunization.process.graph.api.ScheduleGraph;
import org.immunization.schedule.api.Dose;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class SeriesScheduleUtil {

    public static Collection<Dose> getDoses(Dose dose, String seriesCode) {
        if (dose == null) {
            return null;
        }
        ScheduleGraph sg = DefaultResourceService.getInstance().
                getScheduleManager().getSeriesSchedule(seriesCode);
        return sg.getSuccessor(dose);
    }

    public static Collection<Dose> getDoses(Collection<Dose> start, String seriesCode) {
        if (start == null || start.isEmpty()) {
            return null;
        }
        ScheduleGraph sg = DefaultResourceService.getInstance().
                getScheduleManager().getSeriesSchedule(seriesCode);
        ArrayList<Dose> successor = new ArrayList<>();
        for (Dose ds : start) {
            Collection<Dose> dl = sg.getSuccessor(ds);
            if (dl != null) {
                successor.addAll(sg.getSuccessor(ds));
            }
        }
        return successor.isEmpty() ? null : successor;
    }
}
