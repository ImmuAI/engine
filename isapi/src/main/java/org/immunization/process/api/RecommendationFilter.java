/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.api;

import java.time.LocalDate;
import java.util.Collection;
import org.immunization.process.series.api.ProcessReport;
import org.immunization.record.api.impl.ImmunizationReport;
import org.immunization.schedule.api.Shot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public interface RecommendationFilter {

    void process(ImmunizationReport ireport,
            Collection<ProcessReport<Shot<LocalDate>, LocalDate>> seriesCaseReports);
}
