/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api;

import java.util.Collection;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public interface ScheduleCase<D, S> {

    D getDob();

    void setDob(D dob);

    D getEvaluationDate();

    void setEvaluationDate(D evaluationDate);

    Gender getGender();

    void setGender(Gender gender);

    Collection<S> getShots();

    void setShots(Collection<S> shots);
}
