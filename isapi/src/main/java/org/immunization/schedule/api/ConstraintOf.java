/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public enum ConstraintOf {
    EVALUATION_ONLY,
    RECOMMEND_ONLY,
    FEMALE_ONLY,
    MALE_ONLY,
    VACCINE,
    /*recommend same as priviou vaccine*/
    USE_SAME_VACCINE,
    /**/
    LICENSE_DATE_RANGE, //effective date range
    DOB_DATE_RANGE,
    OPTIONAL_GIVEN_BY, //previous dose given before the contraint date, true
    /**/
    MINIMUM_AGE_DATE_RANGE,
    MINIMUM_INTERVAL_RANGE,
    MINIMUM_INTERVAL_TO_PRIOR2_RANGE,
    RECOMMEND_AGE_DATE_RANGE,
    RECOMMEND_INTERVAL_RANGE,
    /**/
    ACC_MINIMUM_INTERVAL_RANGE,
    ACCELERATE_ONLY,
    INC_RISK_GROUP,
    /**/
    /* first choice when recommend*/
    USE_FIRST_VACCINE,
    /**/
    TIMES_USE_IN_SERIES,
    AGE_DATE_RANGE,
    /**/
    USER_DEFINED_0,
    USER_DEFINED_1,
    USER_DEFINED_2,
    USER_DEFINED_3,
    USER_DEFINED_4,
    USER_DEFINED_5,
    USER_DEFINED_6,
    USER_DEFINED_7,
    USER_DEFINED_8,
    USER_DEFINED_9
}
