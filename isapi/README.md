ISAPI contains a library that provides programming interface and default implementation for accepting immunization history of a client, evaluating the history and recommending vaccine the client needs in the near future. 

The API also supports customized rules for processing immunization history. 

ISAPI contains Immunization Schedule defined in a JSON file.


