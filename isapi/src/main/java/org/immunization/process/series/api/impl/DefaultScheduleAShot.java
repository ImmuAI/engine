/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.api.impl;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collection;
import org.immunization.process.series.api.ProcessReport;
import org.immunization.process.series.api.ScheduleAShot;
import org.immunization.process.series.util.AssertConstraint;
import org.immunization.process.series.util.RecommendationShotUtil;
import org.immunization.schedule.api.Constraint;
import org.immunization.schedule.api.ConstraintOf;
import org.immunization.schedule.api.DateRange;
import org.immunization.schedule.api.Dose;
import org.immunization.schedule.api.Gender;
import org.immunization.schedule.api.RecommendedShot;
import org.immunization.schedule.api.VaccineComponent;
import org.immunization.schedule.api.impl.DefaultRecommendedShot;
import org.immunization.schedule.api.impl.DefaultVaccineComponent;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultScheduleAShot implements ScheduleAShot {

    private DefaultProcessReport report;

    @Override
    public void process() {

        Collection<? extends Dose> doses =  report.getSuccessorDose();
        if (doses == null) {
            //throw new IllegalAugumentError()
        }
        ArrayList<RecommendedShot<LocalDate>> shots = new ArrayList<>();
        for (Dose ds : doses) {
            if (report.getEvaluationReport() != null 
                    && !evaluateDoseConstraints(ds)) { 
                if (report.getEvaluationReport().isCompleted()) { //check optional constraint result
                    return;
                }
                continue;
            }
            
            /* create recomendation shot from constraint*/
            DefaultRecommendedShot shot = new DefaultRecommendedShot();
            Constraint<String> c = (Constraint<String>) ds.getConstraint(ConstraintOf.VACCINE);
            VaccineComponent v = new DefaultVaccineComponent();
            v.setCode(c.getConstraint());
            shot.setVaccine(v);
     
            /* get Recommendation Range*/
            DefaultEvaluationReport er = (DefaultEvaluationReport) report.getEvaluationReport();
            DateRange<LocalDate>  dr = 
                    RecommendationShotUtil.createRecommendedDateRange(ds, 
                            er, report.getScheduleCase() );
            if (dr != null) {
                shot.setShotDate(dr.getFrom());
                if (dr.getTo() != null) {
                    shot.setShotDatePeriod(Period.between(dr.getFrom(), dr.getTo()));
                }
                dr = RecommendationShotUtil.createMinimumDateRange(ds, er, report.getScheduleCase());
                if (dr != null) {
                    shot.setMinimumShotDate(dr.getFrom());
                    if (dr.getTo() != null) {
                        shot.setMinimumShotDatePeriod(Period.between(dr.getFrom(), dr.getTo()));
                    }
                    shots.add(shot);
                }
            }
        }
        if (!shots.isEmpty()) {
            DefaultRecommendationReport rpt = 
                    (DefaultRecommendationReport) report.getRecommendationReport();
            rpt.setRecommenedShot(shots);
        }
    }

    private boolean evaluateDoseConstraints(Dose ds) {
       // LocalDate evaluationDate = report.getScheduleCase().getEvaluationDate();      
       DefaultEvaluationReport er; 
       for (Constraint c : ds.getAllConstraint()) {
            boolean passed = true;
            ConstraintOf name = c.getName();
            switch (name) {
                case EVALUATION_ONLY:
                    passed = false;
                    break;
                case FEMALE_ONLY:
                    passed = report.getScheduleCase().getGender().equals(Gender.FEMALE);
                    break;
                case MALE_ONLY:
                    passed = report.getScheduleCase().getGender().equals(Gender.MALE);
                    break;
                case USE_SAME_VACCINE: //last valid shot is Previous Shot 
                    er = (DefaultEvaluationReport) report.getEvaluationReport();
                    passed = AssertConstraint.affirm(c, er.getPreviousValidShot().getVaccine());
                    break;
                case DOB_DATE_RANGE:
                    passed = AssertConstraint.affirm(c, report.getScheduleCase().getDob());
                    break;
                case USE_FIRST_VACCINE:
                    return true;
                default:
            }
            if (!passed) {
                return false;
            }
        }
        return true;
    }

    @Override
    public ProcessReport getProcessReport() {
        return report;
    }

    @Override
    public void setProcessReport(ProcessReport report) {
        this.report = (DefaultProcessReport) report;
    }
}
