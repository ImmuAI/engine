/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.api.impl;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import org.immunization.process.series.api.EvaluationReport;
import org.immunization.schedule.api.Shot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultEvaluationReport implements EvaluationReport<LocalDate> {

    private boolean completed = false;
    // private Collection<? extends Dose> sucessorDose;
    private Collection<Shot<LocalDate>> validShot;
    private HashMap<Shot<LocalDate>, String> invalidShot;
    private int currentDoseNumber = 0;

    @Override
    public boolean isCompleted() {
        return completed;
    }

    @Override
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    protected int getCurrentDoseNumber() {
        return currentDoseNumber;
    }

    protected int getCurrentShotNumber() {
        if (invalidShot == null || invalidShot.isEmpty()) {
            return currentDoseNumber;
        } else {
            return currentDoseNumber + invalidShot.values().size();
        }
    }

    protected void setCurrentDoseNumber(int currentDoseNumber) {
        this.currentDoseNumber = currentDoseNumber;
    }

    @Override
    public Collection<Shot<LocalDate>> getValidShot() {
        return validShot;
    }

    @Override
    public void setValidShot(Collection<Shot<LocalDate>> shots) {
        this.validShot = shots;
    }

    @Override
    public HashMap<Shot<LocalDate>, String> getInvalidShot() {
        return invalidShot;
    }

    @Override
    public void setInvalidShot(HashMap<Shot<LocalDate>, String> shots) {
        invalidShot = shots;
    }

    public Shot<LocalDate> getPreviousValidShot() {
        if (this.validShot == null || validShot.size() < 1) {
            return null;
        }
        List<Shot<LocalDate>> slist = (List<Shot<LocalDate>>) validShot;
        return slist.get(slist.size() - 1);
    }

    public Shot<LocalDate> getShotBeforePreviousValidShot() {
        if (this.validShot == null || validShot.size() < 2) {
            return null;
        }
        List<Shot<LocalDate>> slist = (List<Shot<LocalDate>>) validShot;
        return slist.get(slist.size() - 2);
    }
}
