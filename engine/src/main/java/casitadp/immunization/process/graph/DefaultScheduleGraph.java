/*
 *
 * SPDX-FileCopyrightText: 2020-2022 Weimim Wu <wu.weimin.01@gmail.com>
 * SPDX-FileCopyrightText: 2020-2022 Casita Data Processing LLC
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */

package casitadp.immunization.process.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.function.Supplier;
import org.immunization.process.graph.api.ScheduleGraph;
import org.immunization.schedule.api.Dose;
/* use JGraphT */
import org.jgrapht.graph.*;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultScheduleGraph extends DefaultDirectedGraph<Dose, DefaultEdge>
        implements ScheduleGraph {

    public void addNode(Dose dose) {
        super.addVertex(dose);
    }

    public void removeNode(Dose dose) {
        super.removeVertex(dose);
    }

    public DefaultScheduleGraph() {
        super(DefaultEdge.class);
    }

    public DefaultScheduleGraph(Class<? extends DefaultEdge> edgeClass) {
        super(edgeClass);
    }

    public DefaultScheduleGraph(Supplier<Dose> vertexSupplier, Supplier<DefaultEdge> edgeSupplier, boolean weighted) {
        super(vertexSupplier, edgeSupplier, weighted);
    }

    @Override
    public Collection<Dose> getSuccessor(Dose dose) {
        Set<DefaultEdge> n = super.outgoingEdgesOf​(dose);
        if (n != null) {
            ArrayList<Dose> s = new ArrayList<>();
            n.forEach(edge -> {
                s.add(getEdgeTarget(edge));
            });
            return s;
        }
        return null;
    }

    @Override
    public Collection<Dose> getSuccessor() {
        Set<Dose> doses = super.vertexSet();
        if (doses != null) {
            ArrayList<Dose> s = new ArrayList<>();
            doses.stream().filter(ds -> (inDegreeOf(ds) == 0)).forEachOrdered(ds -> {
                s.add(ds);
            });
            return s.isEmpty() ? null : s;
        }
        return null;
    }

    @Override
    public boolean isLeafDose(Dose dose) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
