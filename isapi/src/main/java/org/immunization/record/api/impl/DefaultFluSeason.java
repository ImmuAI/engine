/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.record.api.impl;

import java.time.Month;
import java.time.MonthDay;
import java.time.Period;
import org.immunization.record.api.FluSeason;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultFluSeason implements FluSeason {

    private MonthDay seasonStart;
    private MonthDay seasonEnd;

    public DefaultFluSeason() {
        seasonStart = MonthDay.of(Month.NOVEMBER, 1);
        seasonEnd = MonthDay.of(Month.APRIL, 1);
    }

    public DefaultFluSeason(MonthDay start, MonthDay end) {
        seasonStart = start;
        seasonEnd = end;
    }

    public Period getFluSeasonDuration() {
        return null;
    }

    @Override
    public MonthDay getFluSeasonStartMonthDay() {
        return seasonStart;
    }

    @Override
    public MonthDay getFluSeasonEndMonthDay() {
        return seasonEnd;
    }

}
