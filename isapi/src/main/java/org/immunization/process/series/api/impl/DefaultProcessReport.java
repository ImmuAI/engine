/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.api.impl;

import java.time.LocalDate;
import java.util.Collection;
import org.immunization.process.series.api.EvaluationReport;
import org.immunization.process.series.api.ProcessReport;
import org.immunization.process.series.api.RecommendationReport;
import org.immunization.schedule.api.Dose;
import org.immunization.schedule.api.ScheduleCase;
import org.immunization.schedule.api.Shot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultProcessReport implements 
        ProcessReport<Shot<LocalDate>, LocalDate> {

    private EvaluationReport<LocalDate> er;
    private RecommendationReport mr;
    private ScheduleCase<LocalDate, Shot<LocalDate>> scase;
    private String scode;
    private Collection<? extends Dose> sucessorDose;
    
    @Override
    public EvaluationReport<LocalDate> getEvaluationReport() {
        return er;
    }

    @Override
    public void setEvaluationReport(EvaluationReport<LocalDate> ereport) {
        er = ereport;
    }

    @Override
    public RecommendationReport getRecommendationReport() {
        return mr;
    }

    @Override
    public void setRecommendationReport(RecommendationReport report) {
        mr = report;
    }

    @Override
    public void setSeries(final String seriesCode) {
        scode = seriesCode;
    }

    @Override
    public String getSeries() {
        return scode;
    }

    @Override
    public void setScheduleCase(ScheduleCase<LocalDate, Shot<LocalDate>> scheduleCase) {
        scase = scheduleCase;
    }

    @Override
    public ScheduleCase<LocalDate, Shot<LocalDate>> getScheduleCase() {
        return scase;
    }
    
    public Collection<? extends Dose> getSuccessorDose() {
        return sucessorDose;
    }

    public void setSuccessorDose(Collection<? extends Dose> dose) {
        sucessorDose = dose;
    }
}
