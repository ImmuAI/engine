/*
 *
 * SPDX-FileCopyrightText: 2020-2022 Weimim Wu <wu.weimin.01@gmail.com>
 * SPDX-FileCopyrightText: 2020-2022 Casita Data Processing LLC
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package casitadp.immunization.resource;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.time.*;
import org.immunization.schedule.api.impl.DefaultDateRange;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public final class Date extends java.util.Date {

    SimpleDateFormat format = null;

    public Date() {
        super();
        format = Util.getDateFormater();
    }

    public Date(java.util.Date date) {
        this();
        this.setTime(date.getTime());
    }

    public Date(String date) throws java.text.ParseException {
        this();
        if (!Util.isValidDate(date)) {
            throw new IllegalArgumentException("Date format should be MM/dd/yyyy.");
        }
        java.util.Date d = format.parse(date);
        if (date.equals(format.format(d))) {
            this.setTime(d.getTime());
        } else {
            throw new IllegalArgumentException("Date format should be MM/dd/yyyy.");
        }
    }

    @Override
    public String toString() {
        return format.format(this);
    }

    /**
     * This method exams if a Date is in DateRrange
     *
     * @param from, start date of the range
     * @param to, end date of the range
     * @return true if the this date is in the range,else return false
     * @throws java.lang.NullPointerException
     */
    public void moveUp(Period ti) {
        this.setTime(Util.addInterval(this, ti).getTime());
    }

    public void moveDown(Period ti) {
        this.setTime(Util.minusInterval(this, ti).getTime());
    }

    public boolean equalTo(java.util.Date date) {
        if (date == null) {
            return false;
        }
        //  return Util.sameDay(this, date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(this);
        Calendar cal0 = Calendar.getInstance();
        cal.setTime(date);
        if (cal.get(Calendar.YEAR) == cal0.get(Calendar.YEAR)
                && cal.get(Calendar.MONTH) == cal0.get(Calendar.MONTH)
                && cal.get(Calendar.DAY_OF_MONTH) == cal0.get(Calendar.DAY_OF_MONTH)) {
            return true;
        }
        return false;
    }

    public static class Util {

        private static final SimpleDateFormat fmter = new SimpleDateFormat("MM/dd/yyyy");
        private static final String usReg =
                "(0[1-9]|1[012])[/](0[1-9]|[1-9]|[12][0-9]|3[01])[/]\\d{4}";

        public static SimpleDateFormat getDateFormater() {
            return fmter;
        }

        public static boolean isValidDate(String date) {
            if (date == null || date.isEmpty()) {
                throw new java.lang.NullPointerException("NULL parameter values.");
            }
            if (date.length() != 10) {
                throw new IllegalArgumentException("Date format should be MM/dd/yyyy.");
            }
            Pattern pattern = Pattern.compile(usReg);
            Matcher matcher = pattern.matcher(date);
            if (!matcher.matches()) {
                return false;
            }
            return true;
        }

        /**
         * Adds a TimeInterval to this date
         *
         * @param ti is TimeInterval will be added to this date
         * @return the date after the time interval is added return null if date
         * is null
         */
        public static java.util.Date addInterval(java.util.Date date, Period ti) {
            if (ti == null) {
                return new java.util.Date(date.getTime());
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            java.util.Date r = new java.util.Date(cal.getTime().getTime());
            return r;
        }

        /**
         * Minus a time interval from this date
         *
         * @return the new date after the time interval is substracted from this
         * date
         * @param ti, the time interval that will be substracted.
         */
        public static java.util.Date minusInterval(java.util.Date date, Period ti) {
            if (ti == null) {
                return new java.util.Date(date.getTime());
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            return new java.util.Date(cal.getTime().getTime());
        }

        public static java.util.Date minusInterval(java.util.Date date, Period ti, int order) {
            if (ti == null) {
                return new java.util.Date(date.getTime());
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
 

            return new java.util.Date(cal.getTime().getTime());
        }

        public static boolean isBefore(Period t1, Period t2) {
            assert t1 != null && t2 != null;
            java.util.Date d = new java.util.Date();
            java.util.Date d1 = addInterval(d, t1);
            java.util.Date d2 = addInterval(d, t2);
            return d1.before(d2);
        }

        public static boolean isAfter(Period t1, Period t2) {
            assert t1 != null && t2 != null;
            java.util.Date d = new java.util.Date();
            java.util.Date d1 = addInterval(d, t1);
            java.util.Date d2 = addInterval(d, t2);
            return d1.after(d2);
        }

        public static boolean sameDay(java.util.Date one, java.util.Date another) {
            if (one == null || another == null) {
                return false;
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(one);
            Calendar cal0 = Calendar.getInstance();
            cal0.setTime(another);
            if (cal.get(Calendar.YEAR) == cal0.get(Calendar.YEAR)
                    && cal.get(Calendar.MONTH) == cal0.get(Calendar.MONTH)
                    && cal.get(Calendar.DAY_OF_MONTH) == cal0.get(Calendar.DAY_OF_MONTH)) {
                return true;
            }
            return false;
        }

        public static boolean isInRange(java.util.Date d, DefaultDateRange range) {//copied from immuni.util in isp
            assert d != null && range != null;
  //          java.util.Date f = range.getFrom() == null ? range.getDefaultFrom() : range.getFrom();
    //        java.util.Date t = range.getTo() == null ? range.getDefaultTo() : range.getTo();
            return false; //!d.before(f) && !d.after(t);
        }
    }
}
