/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.record.api.impl;

import java.time.LocalDate;
import org.immunization.record.api.Shot;
import org.immunization.record.api.Vaccine;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultShot implements Shot<LocalDate>,Comparable<Shot<LocalDate>> {
    private LocalDate shotDate;
    private Vaccine vaccine;

    @Override
    public LocalDate getShotDate() {
        return shotDate;
    }

    @Override
    public void setShotDate(LocalDate date) {
        this.shotDate = date;
    }

    @Override
    public Vaccine getVaccine() {
        return vaccine;
    }

    @Override
    public void setVaccine(Vaccine vaccine) {
        this.vaccine = vaccine;
    }

    @Override
    public int compareTo(Shot<LocalDate> shot) {
        assert shot!=null;
        return this.getShotDate().compareTo(shot.getShotDate());
    }

}
