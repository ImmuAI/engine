/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api.impl;

import java.util.Collection;
import org.immunization.schedule.api.ConstraintOf;
import java.util.EnumMap;
import org.immunization.schedule.api.Constraint;
import org.immunization.schedule.api.Dose;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultDose implements Dose {

    private String nodeId;
    private EnumMap<ConstraintOf, Constraint> constraints;

    @Override
    public void addConstraint(Constraint c) {
        if (constraints == null) {
            constraints = new EnumMap<>(ConstraintOf.class);
        }
        constraints.put(c.getName(), c);
    }

    @Override
    public Constraint getConstraint(ConstraintOf name) {
        if (!hasConstraint(name)) {
            return null;
        }
        return constraints.get(name);
    }

    @Override
    public void removeConstraint(ConstraintOf name) {
        if (hasConstraint(name)) {
            constraints.remove(name);
        }
    }

    @Override
    public String getDoseID() {
        return nodeId;
    }

    @Override
    public void setDoseID(String id) {
        nodeId = id;
    }

    public boolean hasConstraint(ConstraintOf name) {
        if (constraints == null || !constraints.containsKey(name)) {
            return false;
        }
        return true;
    }

    @Override
    public Collection<Constraint> getAllConstraint() {
        return constraints.values();
    }
}
