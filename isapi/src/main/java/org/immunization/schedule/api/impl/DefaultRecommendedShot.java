/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api.impl;

import java.time.LocalDate;
import java.time.Period;
import org.immunization.schedule.api.RecommendedShot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultRecommendedShot extends ScheduleShot 
        implements RecommendedShot<LocalDate> {

    private Period shotDatePeriod;
    private LocalDate minimumShotDate;
    private Period minimumShotDatePeriod;

    @Override
    public void setShotDatePeriod(Period effectivePeriod) {
        shotDatePeriod = effectivePeriod;
    }

    @Override
    public Period getShotDatePeriod() {
        return shotDatePeriod;
    }

    public LocalDate getMinimumShotDate() {
        return minimumShotDate;
    }

    public void setMinimumShotDate(LocalDate shotDate) {
        minimumShotDate = shotDate;
    }

    public void setMinimumShotDatePeriod(Period effectivePeriod) {
        minimumShotDatePeriod = effectivePeriod;
    }

    public Period getMinimumShotDatePeriod() {
        return minimumShotDatePeriod;
    }
}
