/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api.impl;

import java.time.LocalDate;
import org.immunization.schedule.api.Gender;
import org.immunization.schedule.api.ScheduleCase;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public abstract class AbstractScheduleCase<S> implements ScheduleCase<LocalDate, S> {

    private String id;
    private LocalDate dob;
    private Gender gender;
    private LocalDate evaluationDate;
    private boolean enableAccelerationProcess = false;

    protected String getId() {
        return id;
    }

    protected void setId(final String id) {
        this.id = id;
    }

    @Override
    public LocalDate getDob() {
        return dob;
    }

    @Override
    public void setDob(final LocalDate dob) {
        this.dob = dob;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    @Override
    public void setGender(final Gender gender) {
        this.gender = gender;
    }

    @Override
    public LocalDate getEvaluationDate() {
        return evaluationDate;
    }

    @Override
    public void setEvaluationDate(final LocalDate evaluationDate) {
        this.evaluationDate = evaluationDate;
    }

    public boolean isEnableAccelerationProcess() {
        return enableAccelerationProcess;
    }

    public void setEnableAccelerationProcess(boolean enableAccelerationProcess) {
        this.enableAccelerationProcess = enableAccelerationProcess;
    }
}
