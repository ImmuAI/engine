/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api.impl;

import java.time.LocalDate;
import org.immunization.schedule.api.Shot;
import org.immunization.schedule.api.VaccineComponent;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class ScheduleShot implements Shot<LocalDate> {

    private LocalDate shotDate;
    private DefaultVaccineComponent vaccine;

    @Override
    public DefaultVaccineComponent getVaccine() {
        return vaccine;
    }

    @Override
    public LocalDate getShotDate() {
        return shotDate;
    }

    @Override
    public void setVaccine(VaccineComponent vaccine) {
        this.vaccine = (DefaultVaccineComponent) vaccine;
    }

    @Override
    public void setShotDate(LocalDate date) {
        shotDate = date;
    }

    public int compareTo(ScheduleShot shot) {
        return this.getShotDate().compareTo(shot.getShotDate());
    }

}
