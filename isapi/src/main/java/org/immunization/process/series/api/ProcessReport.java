/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.api;

import java.time.LocalDate;
import java.util.Collection;
import org.immunization.schedule.api.Dose;
import org.immunization.schedule.api.ScheduleCase;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public interface ProcessReport<S, D> {

    ScheduleCase<LocalDate, S> getScheduleCase();

    void setScheduleCase(ScheduleCase<LocalDate, S> scheduleCase);

    EvaluationReport<D> getEvaluationReport();

    void setEvaluationReport(EvaluationReport<D> ereport);

    RecommendationReport getRecommendationReport();

    void setRecommendationReport(RecommendationReport report);

    String getSeries();

    void setSeries(String seriesCode);

    Collection<? extends Dose> getSuccessorDose();

    void setSuccessorDose(Collection<? extends Dose> dose);

}
