/*
 *
 * SPDX-FileCopyrightText: 2020-2022 Weimim Wu <wu.weimin.01@gmail.com>
 * SPDX-FileCopyrightText: 2020-2022 Casita Data Processing LLC
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package casitadp.immunization.resource.persistence;

import java.util.List;
import org.immunization.schedule.api.impl.DefaultVaccineComponent;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
class SeriesInJson {
    private String code;
    private String name;
    private List<DoseInJson> doses;
    private List<DefaultVaccineComponent> vaccineComponent;
    private List<DoseConnection> doseConnection;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DoseInJson> getDoses() {
        return doses;
    }

    public void setDoses(List<DoseInJson> doses) {
        this.doses = doses;
    }

    public List<DefaultVaccineComponent> getVaccineComponent() {
        return vaccineComponent;
    }

    public void setVaccineComponent(List<DefaultVaccineComponent> vaccineComponent) {
        this.vaccineComponent = vaccineComponent;
    }

    public List<DoseConnection> getDoseConnection() {
        return doseConnection;
    }

    public void setDoseConnection(List<DoseConnection> doseConnection) {
        this.doseConnection = doseConnection;
    }
    
}
