/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-2.1-or-later
 *
 */

package org.immunization.process.api;

import java.util.Collection;
import org.immunization.record.api.Vaccine;
import org.immunization.schedule.api.VaccineComponent;

/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
public interface VaccineService <V, C>{

    /*service provider name or codeset name*/
    String getName();

    void setName(String value);

    Collection<VaccineComponent> getVaccineComponent(V vaccine);

    Collection<Vaccine> getVaccine(C component);

}
