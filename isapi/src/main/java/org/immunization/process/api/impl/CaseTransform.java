/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.api.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import org.immunization.process.api.Transform;
import org.immunization.process.series.api.ProcessReport;
import org.immunization.process.series.api.impl.DefaultEvaluationReport;
import org.immunization.process.series.api.impl.DefaultProcessReport;
import org.immunization.process.series.api.impl.DefaultRecommendationReport;
import org.immunization.record.api.Vaccine;
import org.immunization.record.api.impl.ImmunizationCase;
import org.immunization.schedule.api.Dose;
import org.immunization.schedule.api.ScheduleCase;
import org.immunization.schedule.api.Shot;
import org.immunization.schedule.api.VaccineComponent;
import org.immunization.schedule.api.impl.ScheduleShot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
class CaseTransform implements
        Transform<ImmunizationCase, Collection<ProcessReport<Shot<LocalDate>, LocalDate>>> {

    @Override
    public Collection<ProcessReport<Shot<LocalDate>, LocalDate>> 
        transform(ImmunizationCase f) throws Exception {
        if (f == null) {
            throw new IllegalArgumentException("Immunization Case cannot be null.");
        }
        HashMap<ScheduleShot, org.immunization.record.api.Shot<LocalDate>> mapping = new HashMap<>();
        /*transform record shots to schedule shots first*/
        List<ScheduleShot> componentShot = null;
        if (f.getShots() != null && !f.getShots().isEmpty()) {
            componentShot = new ArrayList<>();
            for (org.immunization.record.api.Shot<LocalDate> s : f.getShots()) {
                for (VaccineComponent c : getComponent(s.getVaccine())) {
                    ScheduleShot shot = new ScheduleShot();
                    shot.setShotDate(s.getShotDate());
                    shot.setVaccine(c);
                    componentShot.add(shot);
                    /* update mapping for later transform back*/
                    mapping.put(shot, s);
                }
            }
        }

        /* then create ProcessReport with ScheduleCase, 
        * Recommendation Report for each Series
         */
        // ArrayList<ProcessReport<Shot<LocalDate>>> reports = new ArrayList<>();
        HashMap<String, ProcessReport<Shot<LocalDate>, LocalDate>> 
                reports = new HashMap<>();
        getSeries().forEach(s -> {
            DefaultProcessReport report = new DefaultProcessReport();
            ScheduleCase<LocalDate, Shot<LocalDate>> scase
                    = new DefaultScheduleCase(f);

            report.setScheduleCase(scase);
            report.setSeries(s);
            report.setRecommendationReport(new DefaultRecommendationReport());
            report.setSuccessorDose(getSucessor(s));
            reports.put(s, report);
        });
        /* Then if there is any shot for a Series, then add shots to the Schedule Case,
        * and add a new Evaluation Report
         */
        if (componentShot != null) {
            for (ScheduleShot shot : componentShot) {
                String series = shot.getVaccine().getSeriesCode();
                ProcessReport<Shot<LocalDate>, LocalDate> sreport = reports.get(series);

                if (sreport.getEvaluationReport() == null) {
                    /*need evaluation, init Evaluation report, and add shot to Schedule Case*/
                    sreport.setEvaluationReport(new DefaultEvaluationReport());
                    ArrayList<Shot<LocalDate>> shots = new ArrayList<>();
                    shots.add(shot);
                    sreport.getScheduleCase().setShots(shots);
                } else {
                    sreport.getScheduleCase().getShots().add(shot);
                }

                /* shot mapping */
                DefaultRecommendationReport r = (DefaultRecommendationReport) sreport.getRecommendationReport();
                if (r.getShotMapping() == null) {
                    HashMap<ScheduleShot, org.immunization.record.api.Shot<LocalDate>> 
                            smapping = new HashMap<>();
                    r.setShotMapping(smapping);
                    smapping.put(shot, mapping.get(shot));
                }
                r.getShotMapping().put(shot, mapping.get(shot));
            }
        }
        return reports.values();
    }

    private Collection<VaccineComponent> getComponent(Vaccine vaccine) throws IllegalArgumentException{
        Collection<VaccineComponent> vc = DefaultResourceService.getInstance()
                .getVaccineManager().getVaccineComponent(vaccine.getCode()); 
        if (vc == null) {
            throw new IllegalArgumentException(vaccine.getName() + ": Vaccine not found.");
        }
        return vc;
    }

    private Collection<String> getSeries() {
        DefaultScheduleService service = (DefaultScheduleService) DefaultResourceService.getInstance()
                .getScheduleManager();
        return service.getAllSeries();
    }

    private Collection<Dose> getSucessor(String scode) {
        DefaultScheduleService service = (DefaultScheduleService) DefaultResourceService.getInstance()
                .getScheduleManager();
        return service.getRootDoses(scode);
    }
}
