/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.api.impl;

import java.util.Collection;
import java.util.HashMap;
import org.immunization.process.api.ScheduleService;
import org.immunization.process.graph.api.ScheduleGraph;
import org.immunization.schedule.api.Dose;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultScheduleService implements ScheduleService {

    private final HashMap<String, ScheduleGraph> sgraph;
    private final HashMap<String, Collection<Dose>> rootDoses;

    public DefaultScheduleService() {
        this.sgraph = new HashMap<>();
        this.rootDoses = new HashMap<>();
    }

    @Override
    public ScheduleGraph getSeriesSchedule(String key) {
        return sgraph.get(key);
    }

    @Override
    public void setSeriesSchedule(String key, ScheduleGraph schedule) {
        sgraph.put(key, schedule);
        rootDoses.put(key, schedule.getSuccessor());
    }

    public Collection<Dose> getRootDoses(String key) {
        return rootDoses.get(key);
    }

    public Collection<String> getAllSeries() {
        return sgraph.keySet();
    }
}
