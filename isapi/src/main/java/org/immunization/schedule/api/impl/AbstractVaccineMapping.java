/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api.impl;

import java.util.ArrayList;
import org.immunization.record.api.Vaccine;
import org.immunization.schedule.api.VaccineComponent;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class AbstractVaccineMapping {

    private Vaccine vaccine;
    private ArrayList<VaccineComponent> components;
    
    public ArrayList<VaccineComponent> getComponents() {
        return components;
    }

    public void setComponents(ArrayList<VaccineComponent> components) {
        this.components = components;
    }

    public Vaccine getVaccine() {
        return vaccine;
    }

    public void setVaccine(Vaccine vaccine) {
        this.vaccine = vaccine;
    }
}
