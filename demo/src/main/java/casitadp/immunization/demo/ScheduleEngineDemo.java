/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package casitadp.immunization.demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.immunization.process.api.impl.DefaultScheduleProcessService;
import org.immunization.record.api.RecommendationShot;
import org.immunization.record.api.impl.DefaultShot;
import org.immunization.record.api.impl.DefaultVaccine;
import org.immunization.record.api.impl.ImmunizationCase;
import org.immunization.record.api.impl.ImmunizationReport;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class ScheduleEngineDemo {

    private DefaultScheduleProcessService ps;

    public ScheduleEngineDemo() {
        init();
    }

    private void init() {
        new ImmunizationService();
        ps = new DefaultScheduleProcessService();
    }

    private ImmunizationReport demo(ImmunizationCase immCase) {
        /* transform to series cases, stored in process report*/
        ImmunizationReport report = ps.process(immCase);
        return report;
    }

    private ImmunizationCase createDemoImmunizationCase() {
        ImmunizationCase one = new ImmunizationCase();
        one.setDob(LocalDate.now().minusYears(10));
        one.setEvaluationDate(LocalDate.from(one.getDob()).plusWeeks(1));

        ArrayList<org.immunization.record.api.Shot<LocalDate>> shots = new ArrayList<>();
        DefaultShot shot = new DefaultShot();
        DefaultVaccine v = new DefaultVaccine();
        v.setCode("46021");
        v.setName("ENGERIX-B pediadric");
        shot.setShotDate(LocalDate.from(one.getDob()).plusMonths(2));
        shot.setVaccine(v);
        shots.add(shot);
        one.setShots(shots);

        return one;
    }

    private void printOutDemoReport(ImmunizationReport report) {
        assert report != null;
        ArrayList<String> s = new ArrayList<>();
        if (report.getShots() != null) {
            s.addAll(report.getShots().keySet());
        }
        Collection<String> cs = report.getCompletedSeries();
        LocalDate cutoffdate = report.getRecord().getEvaluationDate().plusMonths(12);
        if (cs != null) {
            System.out.println("Completed series:");
            System.out.println(cs);
            s.removeAll(cs);
        }
        if (report.getCombinedVaccineShots() != null) {
            System.out.println("\n");
            System.out.println("Recommended Combined Vaccine Shots:");
            for (RecommendationShot rshot : report.getCombinedVaccineShots()) {
                System.out.println(toShotString(rshot));
            }
            s.removeAll(report.getCombinedVaccineShotSeries());
        }
        if (report.getRecommendationShots() != null) {
            System.out.println("\n");
            System.out.println("Recommended shot per immunization history:");
            for (String series : report.getRecommendationShots().keySet()) {
                if (s.contains(series)) {
                    System.out.println(toShotString(report.getRecommendationShots().get(series)));
                    s.remove(series);
                }
            }
        }
        if (report.getShots() != null) {
            System.out.println("\n");
            System.out.println("Recommended shot based on shot date:");
            StringBuilder sb = new StringBuilder();
            for (String series : report.getShots().keySet()) {
                if (s.contains(series)) {
                    List<RecommendationShot> seriesRecommendation
                            = (List<RecommendationShot>) report.getShots().get(series);
                    RecommendationShot rshot = (RecommendationShot) seriesRecommendation.get(0);
                    if (rshot.getShotDate().isBefore(cutoffdate)) {
                        System.out.println("series code-" + series + " need a shot at or after "
                                + rshot.getShotDate().toString() + " from vaccine:");
                        for (Object aShot : seriesRecommendation) {
                            RecommendationShot shot = (RecommendationShot) aShot;
                            if (shot.getShotDate().isAfter(rshot.getShotDate())) {
                                break;
                            }
                            System.out.println(shot.getVaccine().getName());
                        }
                    } else {
                        sb.append("series code-" + series).append(" no shot needed in next 12 month.").append("\n");
                    }
                }
            }
            System.out.println("\n");
            System.out.println(sb);
        }
        if (cs == null || !cs.contains("1501")) {
            System.out.println(1501);
            System.out.println("need flu shot in Flu Season");
        }
    }

    private String toShotString(RecommendationShot rshot) {
        return rshot.getVaccine().getName() + " "
                + rshot.getShotDate().toString();
    }

    public static void main(String[] args) {
        ScheduleEngineDemo demo = new ScheduleEngineDemo();
        ImmunizationCase demoCase
                = demo.createDemoImmunizationCase();

        ImmunizationReport report = demo.demo(demoCase);
        demo.printOutDemoReport(report);
    }
}
