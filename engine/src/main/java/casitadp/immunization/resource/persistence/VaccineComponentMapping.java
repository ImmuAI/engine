/*
 *
 * SPDX-FileCopyrightText: 2020-2022 Weimim Wu <wu.weimin.01@gmail.com>
 * SPDX-FileCopyrightText: 2020-2022 Casita Data Processing LLC
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package casitadp.immunization.resource.persistence;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
class VaccineComponentMapping {
    private String vaccine;
    private String component;

    public String getVaccine() {
        return vaccine;
    }

    public void setVaccine(String vaccine) {
        this.vaccine = vaccine;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }
    
}
