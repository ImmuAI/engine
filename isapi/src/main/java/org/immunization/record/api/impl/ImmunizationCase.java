/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.record.api.impl;

import java.time.LocalDate;
import java.util.Collection;
import org.immunization.record.api.Shot;
import org.immunization.schedule.api.impl.AbstractScheduleCase;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class ImmunizationCase extends AbstractScheduleCase<Shot<LocalDate>> {

    private Collection<Shot<LocalDate>> shots;
 
    @Override
    protected String getId() {
        return super.getId();
    }

    @Override
    protected void setId(String id) {
        super.setId(id);
    }

    @Override
    public Collection<Shot<LocalDate>> getShots() {
        return shots;
    }

    @Override
    public void setShots(Collection<Shot<LocalDate>> shots) {
        this.shots = shots;
    }
}
