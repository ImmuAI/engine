/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.flu.api.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.immunization.process.series.api.ProcessReport;
import org.immunization.process.series.api.impl.DefaultEvaluationReport;
import org.immunization.process.series.api.impl.DefaultRecommendationReport;
import org.immunization.process.series.api.impl.SeriesProcessor;
import org.immunization.process.series.flu.api.FluSeriesProcessor;
import org.immunization.record.api.FluSeason;
import org.immunization.schedule.api.DateRange;
import org.immunization.schedule.api.RecommendedShot;
import org.immunization.schedule.api.Shot;
import org.immunization.schedule.api.impl.DefaultDateRange;
import org.immunization.schedule.api.impl.DefaultRecommendedShot;
import org.immunization.schedule.api.impl.DefaultVaccineComponent;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class SimpleFluSeriesProcessor implements FluSeriesProcessor {
    
    private ProcessReport<Shot<LocalDate>, LocalDate> report;
    private FluSeason fs;
    private SeriesProcessor processor;
    
    @Override
    public void process() {
        assert report.getScheduleCase() == null;
        if (report.getScheduleCase().getShots() != null) {
            Collection<Shot<LocalDate>> shots = report.getScheduleCase().getShots();
            evaluate(shots);
        } else {
            needShotInCurrentFluSeason();
        }
    }
    
    private void evaluate(Collection<Shot<LocalDate>> shots) {
        List<Shot<LocalDate>> sl = (List<Shot<LocalDate>>) shots;
        Shot<LocalDate> lastShot = sl.get(sl.size() - 1);
        LocalDate sd = lastShot.getShotDate(); //last shot before evaluation date
        DateRange<LocalDate> currentSeason
                = getFluSeasonOfDate(report.getScheduleCase().getEvaluationDate());
        if (!sd.isBefore(currentSeason.getFrom()) //current season
                && !sd.isAfter(currentSeason.getTo())) {
            if (sl.size() == 1) {
                evaluateOneShot();
            } else {
                getEvaluationReport().setCompleted(true);
            }
        } else {
            //getEvaluationReport().setCompleted(false);
            needShotInCurrentFluSeason();
        }
    }
    
    private void needShotInCurrentFluSeason() {
        DefaultRecommendationReport rpt = getRecommendationReport();
        DefaultRecommendedShot shot = new DefaultRecommendedShot();
        DefaultVaccineComponent vc = new DefaultVaccineComponent();
        vc.setName("flu vaccine");
        shot.setVaccine(vc);
        shot.setShotDate(report.getScheduleCase().getEvaluationDate());
        ArrayList<RecommendedShot<LocalDate>> l = new ArrayList<>();
        l.add(shot);
        rpt.setRecommenedShot(l);
    }
    
    private void evaluateOneShot() {
        processor.setReport(report);
        processor.process();
        List<RecommendedShot<LocalDate>> rl = 
                (List<RecommendedShot<LocalDate>>) getRecommendationReport().getRecommenedShot();
        if (rl != null) {
            List<RecommendedShot<LocalDate>> rshots = new ArrayList<>();
            DateRange<LocalDate> currentSeason
                    = getFluSeasonOfDate(report.getScheduleCase().getEvaluationDate());
            for (RecommendedShot<LocalDate> shot : rl) {
                if (shot.getShotDate().isBefore(currentSeason.getTo())) {
                    if (shot.getShotDate().isBefore(currentSeason.getFrom())) {
                        shot.setShotDate(LocalDate.from(currentSeason.getFrom()));
                    }
                    rshots.add(shot);
                }
            }
            if (rshots.size() > 0) {
                getRecommendationReport().setRecommenedShot(rshots);
            }
        }
    }
    
    @Override
    public FluSeason getFluSeason() {
        return fs;
    }
    
    @Override
    public void setFluSeason(FluSeason fs) {
        this.fs = fs;
    }
    
    private DateRange<LocalDate> getFluSeasonOfDate(LocalDate date) {
        LocalDate adjShotDate = date.minusMonths(6);
        int year = adjShotDate.getYear();
        LocalDate f = LocalDate.of(year,
                fs.getFluSeasonStartMonthDay().getMonthValue(),
                fs.getFluSeasonStartMonthDay().getDayOfMonth());
        LocalDate e = LocalDate.of(year + 1,
                fs.getFluSeasonEndMonthDay().getMonthValue(),
                fs.getFluSeasonEndMonthDay().getDayOfMonth());
        return new DefaultDateRange(f, e);
    }

    /* exam if a shot date is in */
    private boolean isDateInFluSeason(LocalDate date) {
        DateRange<LocalDate> dr = getFluSeasonOfDate(date);
        if (!date.isBefore(dr.getFrom()) && !date.isAfter(dr.getTo())) {
            return true;
        }
        return false;
    }

    /* cast the Evaluation report*/
    private DefaultEvaluationReport getEvaluationReport() {
        return (DefaultEvaluationReport) report.getEvaluationReport();
    }
    
    private DefaultRecommendationReport getRecommendationReport() {
        return (DefaultRecommendationReport) report.getRecommendationReport();
    }
    
    @Override
    public void setReport(ProcessReport<Shot<LocalDate>, LocalDate> report) {
        this.report = report;
    }
    
    @Override
    public ProcessReport<Shot<LocalDate>,LocalDate> getReport() {
        return report;
    }
    
    public void setProcessor(SeriesProcessor processor) {
        this.processor = processor;
    }
    
}
