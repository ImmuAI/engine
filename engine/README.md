Engine contains a library that implements the API in ISAPI for reading Immunization Schedule defined in a JSON file, storing the Immunization Schedule and responsing request of finding Dose information in the schedule. 

The library depends json persistence library, because the immunization schedule in ISAPI package is in a JSON file. The library depends on Graph library, as it stores the immunization schedule in Graph struction, by design.  


