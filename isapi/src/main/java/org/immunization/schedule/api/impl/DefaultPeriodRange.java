/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api.impl;

import java.time.*;
import java.util.Date;
import org.immunization.schedule.api.DateRange;
import org.immunization.schedule.api.PeriodRange;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultPeriodRange implements PeriodRange<Period> {

    private Period from;
    private Period to;
    private boolean isFromIncluded = true;
    private boolean isToIncluded = false;

    public DefaultPeriodRange() {
    }

    public DefaultPeriodRange(Period from) {
        this.from = from;
        this.to = null;
    }

    public DefaultPeriodRange(Period from, Period to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public boolean isEmpty() {
        if (from == null && to == null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Period getFrom() {
        return from;
    }

    @Override
    public boolean isValid() {
        if (from != null && to == null) {
            return true;
        }
        if (from != null && to != null) {
            Date base = new Date();
            Date df = Date.from(base.toInstant()
                    .atZone(ZoneId.systemDefault()).plus(from).toInstant());
            Date dt = Date.from(base.toInstant()
                    .atZone(ZoneId.systemDefault()).plus(to).toInstant());
            if (dt.before(df)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void setFrom(Period from) {
        this.from = from;
    }

    public boolean isFromBoundaryIncluded() {
        return isFromIncluded;
    }

    public void setFromIncluded(boolean isFromIncluded) {
        this.isFromIncluded = isFromIncluded;
    }

    public boolean isToBoundaryIncluded() {
        return isToIncluded;
    }

    public void setToIncluded(boolean isToIncluded) {
        this.isToIncluded = isToIncluded;
    }

    @Override
    public Period getTo() {
        return to;
    }

    @Override
    public void setTo(Period to) {
        this.to = to;
    }

    protected DateRange<LocalDate> toDateRange(LocalDate d) {
        LocalDate f = d.plus(from);
        LocalDate t = d.plus(to);
        return new DefaultDateRange(f, t);
    }
}
