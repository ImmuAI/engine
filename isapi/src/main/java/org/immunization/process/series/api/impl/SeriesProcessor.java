/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.api.impl;

import java.time.LocalDate;
import java.util.Collection;
import org.immunization.process.series.api.EvaluateAShot;
import org.immunization.process.series.api.ProcessReport;
import org.immunization.process.series.api.ScheduleAShot;
import org.immunization.schedule.api.Shot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class SeriesProcessor {

    private ProcessReport<Shot<LocalDate>, LocalDate> report;
    private EvaluateAShot<ProcessReport<Shot<LocalDate>, LocalDate>> evaluateAShot;
    private ScheduleAShot recommendAShot;

    public void process() {
        assert report.getScheduleCase() == null;
        if (report.getScheduleCase().getShots() != null) {
            Collection<Shot<LocalDate>> shots = report.getScheduleCase().getShots();
            evaluate(shots);
        }

        if (report.getScheduleCase().getShots() == null
                || !getEvaluationReport().isCompleted()) {
            recommendAShot.process();
        }
    }

    /* evaluate each shot*/
    private void evaluate(Collection<Shot<LocalDate>> shots) {
        getEvaluationReport().setCurrentDoseNumber(0);
        for (int i = 0; i < shots.size(); i++) {
           // int doseNumber = getEvaluationReport().getCurrentDoseNumber();
            evaluateAShot.process();
            /**/
            if (getEvaluationReport().isCompleted()) {
                return;
            }
        }
    }

    public EvaluateAShot<
        ProcessReport<Shot<LocalDate>, LocalDate>> getEvaluateShot() {
        return evaluateAShot;
    }

    public void setEvaluateAShot(
            EvaluateAShot<ProcessReport<Shot<LocalDate>, LocalDate>> evaluator) {
        this.evaluateAShot = evaluator;
    }

    public ScheduleAShot getRecommendAShot() {
        return recommendAShot;
    }

    public void setScheduleAShot(ScheduleAShot scheduler) {
        this.recommendAShot = scheduler;
    }

    public ProcessReport<Shot<LocalDate>,LocalDate> getReport() {
        return report;
    }

    public void setReport(ProcessReport<Shot<LocalDate>,LocalDate> report) {
        this.report = report;
        evaluateAShot.setProcessReport(report);
        recommendAShot.setProcessReport(report);
    }

    /* cast the Evaluation report*/
    private DefaultEvaluationReport getEvaluationReport() {
        return (DefaultEvaluationReport) report.getEvaluationReport();
    }

    private DefaultRecommendationReport getRecommendationReport() {
        return (DefaultRecommendationReport) report.getRecommendationReport();
    }
}
