/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.api.impl;

import java.util.Collection;
import java.util.HashSet;
import org.immunization.process.series.api.DoseFilter;
import org.immunization.schedule.api.ConstraintOf;
import org.immunization.schedule.api.Dose;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DoseFilterByConstraint implements DoseFilter {

    @Override
    public Collection<? extends Dose> filter(Collection<? extends Dose> doses) {
        if (doses == null) {
            return null;
        }
        HashSet<Dose> l = new HashSet<>();
        for (Dose ds : doses) {
            if (ds.getConstraint(ConstraintOf.INC_RISK_GROUP) != null
                    || ds.getConstraint(ConstraintOf.ACCELERATE_ONLY) != null) {
                continue;
            }
            l.add(ds);
        }
        return l;
    }

}
