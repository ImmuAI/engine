/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.record.api;

import java.time.LocalDate;
import java.time.Period;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public interface RecommendationShot extends Shot<LocalDate>{

    Vaccine getVaccine();

    void setVaccine(final Vaccine vaccine);

    void setShotDatePeriod(Period effectivePeriod);

    Period getShotDatePeriod();

    String getComment();

    void setComment(final String comment);
}
