/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api.impl;

import org.immunization.schedule.api.VaccineComponent;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultVaccineComponent implements VaccineComponent {

    private String name;
    private String code;
    private String seriesCode;
    private boolean liveAttenuated = false;

    public String getSeriesCode() {
        return seriesCode;
    }

    public void setSeriesCode(String seriesCode) {
        this.seriesCode = seriesCode;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean isLiveAttenuated() {
        return liveAttenuated;
    }

    @Override
    public void setLiveAttenuated(boolean liveAttenuated) {
        this.liveAttenuated = liveAttenuated;
    }
}
