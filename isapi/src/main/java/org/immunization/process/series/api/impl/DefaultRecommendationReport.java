/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.api.impl;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import org.immunization.process.series.api.RecommendationReport;
import org.immunization.record.api.RecommendationShot;
import org.immunization.record.api.Shot;
import org.immunization.schedule.api.RecommendedShot;
import org.immunization.schedule.api.impl.ScheduleShot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultRecommendationReport implements RecommendationReport {

    private Collection<RecommendedShot<LocalDate>> shots;
    private HashMap<ScheduleShot, Shot<LocalDate>> smapping;

    public HashMap<ScheduleShot, Shot<LocalDate>> getShotMapping() {
        return smapping;
    }

    public void setShotMapping(HashMap<ScheduleShot, Shot<LocalDate>> smapping) {
        this.smapping = smapping;
    }

    @Override
    public Collection<RecommendationShot> firstRecommendationShots() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<RecommendationShot> nextRecommendationShots() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Collection<RecommendedShot<LocalDate>> getRecommenedShot() {
        return shots;
    }

    public void setRecommenedShot(Collection<RecommendedShot<LocalDate>> rshot) {
        shots = rshot;
    }
}
