/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.api;

import java.util.Collection;
import java.util.HashMap;
import org.immunization.schedule.api.Shot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public interface EvaluationReport <D> {

    boolean isCompleted();

    void setCompleted(boolean completed);

  //  public Collection<? extends Dose> getFirstDoseSet();

  //  public void setFirstDoseSet(Collection<? extends Dose> dose);

    Collection<Shot<D>> getValidShot();

    void setValidShot(Collection<Shot<D>> shots);

    HashMap<Shot<D>, String> getInvalidShot();

    void setInvalidShot(HashMap<Shot<D>, String> shots);
}
