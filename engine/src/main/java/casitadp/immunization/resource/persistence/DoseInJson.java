/*
 *
 * SPDX-FileCopyrightText: 2020-2022 Weimim Wu <wu.weimin.01@gmail.com>
 * SPDX-FileCopyrightText: 2020-2022 Casita Data Processing LLC
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package casitadp.immunization.resource.persistence;

import org.immunization.schedule.api.impl.DefaultDateRange;
import org.immunization.schedule.api.impl.DefaultPeriodRange;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
class DoseInJson {

    private String id;
    private String vaccine;
    private DefaultDateRange dobRange;
    private DefaultDateRange effectRange;

    private DefaultPeriodRange ageRange;
    private DefaultPeriodRange minAgeRange;
    private DefaultPeriodRange RecommendedAgeRange;
    private DefaultPeriodRange minIntervalRange1;
    private DefaultPeriodRange minIntervalRange;
    private DefaultPeriodRange recommIntervalRange;
    private DefaultPeriodRange accelerateIntervalRange;
    private DefaultPeriodRange optDoseGivenBy;

    private boolean riskGroup = false;
    private boolean evaluationOnly = false;
    private boolean femaleOnly = false;
    private String optimalVaccine;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVaccine() {
        return vaccine;
    }

    public void setVaccine(String vaccine) {
        this.vaccine = vaccine;
    }

    public DefaultDateRange getDobRange() {
        return dobRange;
    }

    public void setDobRange(DefaultDateRange dobRange) {
        this.dobRange = dobRange;
    }

    public DefaultDateRange getEffectRange() {
        return effectRange;
    }

    public void setEffectRange(DefaultDateRange effectRange) {
        this.effectRange = effectRange;
    }

    public DefaultPeriodRange getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(DefaultPeriodRange ageRange) {
        this.ageRange = ageRange;
    }

    public DefaultPeriodRange getMinAgeRange() {
        return minAgeRange;
    }

    public void setMinAgeRange(DefaultPeriodRange minAgeRange) {
        this.minAgeRange = minAgeRange;
    }

    public DefaultPeriodRange getRecommendedAgeRange() {
        return RecommendedAgeRange;
    }

    public void setRecommendedAgeRange(DefaultPeriodRange RecommendedAgeRange) {
        this.RecommendedAgeRange = RecommendedAgeRange;
    }

    public DefaultPeriodRange getMinIntervalRange1() {
        return minIntervalRange1;
    }

    public void setMinIntervalRange1(DefaultPeriodRange minIntervalRange1) {
        this.minIntervalRange1 = minIntervalRange1;
    }

    public DefaultPeriodRange getMinIntervalRange() {
        return minIntervalRange;
    }

    public void setMinIntervalRange(DefaultPeriodRange minIntervalRange) {
        this.minIntervalRange = minIntervalRange;
    }

    public DefaultPeriodRange getRecommIntervalRange() {
        return recommIntervalRange;
    }

    public void setRecommIntervalRange(DefaultPeriodRange recommIntervalRange) {
        this.recommIntervalRange = recommIntervalRange;
    }

    public boolean isRiskGroup() {
        return riskGroup;
    }

    public void setRiskGroup(boolean riskGroup) {
        this.riskGroup = riskGroup;
    }

    public boolean isEvaluationOnly() {
        return evaluationOnly;
    }

    public void setEvaluationOnly(boolean evaluationOnly) {
        this.evaluationOnly = evaluationOnly;
    }

    public boolean isFemaleOnly() {
        return femaleOnly;
    }

    public void setFemaleOnly(boolean femaleOnly) {
        this.femaleOnly = femaleOnly;
    }

    public DefaultPeriodRange getAccelerateIntervalRange() {
        return accelerateIntervalRange;
    }

    public void setAccelerateIntervalRange(DefaultPeriodRange accelerateIntervalRange) {
        this.accelerateIntervalRange = accelerateIntervalRange;
    }

    public DefaultPeriodRange getOptDoseGivenBy() {
        return optDoseGivenBy;
    }

    public void setOptDoseGivenBy(DefaultPeriodRange optDoseGivenBy) {
        this.optDoseGivenBy = optDoseGivenBy;
    }

    public String getOptimalVaccine() {
        return optimalVaccine;
    }

    public void setOptimalVaccine(String optimalVaccine) {
        this.optimalVaccine = optimalVaccine;
    }

}
