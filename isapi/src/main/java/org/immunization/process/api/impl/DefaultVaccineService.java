/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.api.impl;

import java.util.Collection;
import java.util.HashMap;
import org.immunization.process.api.VaccineService;
import org.immunization.record.api.Vaccine;
import org.immunization.schedule.api.VaccineComponent;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultVaccineService implements VaccineService<String, String> {

    private HashMap<String, Collection<VaccineComponent>> vaccines;
    private HashMap<String, Collection<Vaccine>> vaccineComponents;
    private HashMap<String, Vaccine> vaccineByCode;
    private HashMap<String, VaccineComponent> componentByCode;
    private String name = "ACIP vaccine";
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String value) {
        this.name = value;
    }

    @Override
    public Collection<VaccineComponent> getVaccineComponent(String vaccine) {
        return vaccines.get(vaccine);
    }

    @Override
    public Collection<Vaccine> getVaccine(String component) {
        return vaccineComponents.get(component);
    }

    public HashMap<String, Collection<VaccineComponent>> getVaccines() {
        return vaccines;
    }

    public void setVaccines(HashMap<String, Collection<VaccineComponent>> vaccines) {
        this.vaccines = vaccines;
    }

    public HashMap<String, Collection<Vaccine>> getVaccineComponents() {
        return vaccineComponents;
    }

    public void setVaccineComponents(HashMap<String, Collection<Vaccine>> vaccineComponents) {
        this.vaccineComponents = vaccineComponents;
    }

    public HashMap<String, Vaccine> getVaccineByCode() {
        return vaccineByCode;
    }

    public void setVaccineByCode(HashMap<String, Vaccine> vaccineByCode) {
        this.vaccineByCode = vaccineByCode;
    }

    public HashMap<String, VaccineComponent> getComponentByCode() {
        return componentByCode;
    }

    public void setComponentByCode(HashMap<String, VaccineComponent> componentByCode) {
        this.componentByCode = componentByCode;
    }

}
