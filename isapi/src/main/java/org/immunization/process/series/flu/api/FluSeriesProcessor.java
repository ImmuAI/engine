/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.flu.api;

import java.time.LocalDate;
import org.immunization.process.series.api.ProcessReport;
import org.immunization.record.api.FluSeason;
import org.immunization.schedule.api.Shot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public interface FluSeriesProcessor {

    public void process();

    public void setReport(ProcessReport<Shot<LocalDate>, LocalDate> report);

    public ProcessReport<Shot<LocalDate>, LocalDate> getReport();

    public FluSeason getFluSeason();

    public void setFluSeason(FluSeason fs);

}
