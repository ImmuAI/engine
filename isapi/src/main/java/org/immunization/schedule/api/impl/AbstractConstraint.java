/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.schedule.api.impl;

import org.immunization.schedule.api.Constraint;
import org.immunization.schedule.api.ConstraintOf;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class AbstractConstraint<T> implements Constraint<T>, Comparable<Constraint<T>> {
    private T constraint;
    private ConstraintOf name;

    public AbstractConstraint(ConstraintOf name, T constraint) {
        this.constraint = constraint;
        this.name = name;
    }

    @Override
    public T getConstraint() {
        return constraint;
    }

    @Override
    public void setConstraint(T constraint) {
        this.constraint = constraint;
    }

    @Override
    public ConstraintOf getName() {
        return name;
    }

    @Override
    public void setName(ConstraintOf name) {
        this.name = name;
    }

    @Override
    public int compareTo(Constraint<T> arg0) {
        return arg0.getName().compareTo(name);
    }
}
