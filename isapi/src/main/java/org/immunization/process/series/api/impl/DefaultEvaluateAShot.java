/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.series.api.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.immunization.process.series.api.DoseFilter;
import org.immunization.process.series.api.EvaluateAShot;
import org.immunization.process.series.api.EvaluationReport;
import org.immunization.process.series.util.AssertConstraint;
import org.immunization.process.series.util.SeriesScheduleUtil;
import org.immunization.schedule.api.Constraint;
import org.immunization.schedule.api.ConstraintOf;
import org.immunization.schedule.api.Dose;
import org.immunization.schedule.api.Gender;
import org.immunization.schedule.api.Shot;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
public class DefaultEvaluateAShot implements EvaluateAShot<DefaultProcessReport> {

    private DefaultProcessReport report;
    private DoseFilter filter;
 
    @Override
    public void process() {
        Collection<? extends Dose> doses = report.getSuccessorDose();
        doses = filter.filter(doses);
        if (doses == null) {
            return;
        }
        EvaluationReport<LocalDate> er = report.getEvaluationReport();
        //  report.getShot();
        Shot<LocalDate> shot = getCurrentShot();
        while (shot != null) {
            Collection<? extends Dose> validDoses;
            validDoses = processOneShot(shot, doses);
            if (er.isCompleted()){
                break;
            }else if (validDoses == null) {
                if (er.getInvalidShot() == null){
                    er.setInvalidShot(new HashMap<>());
                }
                if (!er.getInvalidShot().containsKey(shot)){
                    er.getInvalidShot().put(shot, "Invalid shot");
                }
                shot = getCurrentShot();
            }else {
                report.setSuccessorDose(validDoses);
                ((DefaultEvaluationReport)report.getEvaluationReport()).setCurrentDoseNumber(
                    ((DefaultEvaluationReport)report.getEvaluationReport()).getCurrentDoseNumber() + 1);
                break;
            }
        }
    }
    
    private Collection<? extends Dose> processOneShot(Shot<LocalDate> shot, 
            Collection<? extends Dose>  doses){
        Set<Dose> validDoses = new HashSet<>();  
        DefaultEvaluationReport er = (DefaultEvaluationReport) report.getEvaluationReport();
        for (Dose ds : doses) {
            boolean passed = true;
            ConstraintOf name = null;
            
            for (Constraint c : ds.getAllConstraint()) {
                name = c.getName();

                switch (name) {
                    case RECOMMEND_ONLY:
                        passed = false;
                        break;
                    case FEMALE_ONLY:
                        passed = report.getScheduleCase().getGender().equals(Gender.FEMALE);
                        break;
                    case MALE_ONLY:
                        passed = report.getScheduleCase().getGender().equals(Gender.MALE);
                        break;
//case USE_SAME_VACCINE:
                    case VACCINE:
                        passed = AssertConstraint.affirm(c, shot.getVaccine().getCode());
                        break;
                    case DOB_DATE_RANGE:
                        passed = AssertConstraint.affirm(c, report.getScheduleCase().getDob());
                        break;
                    case LICENSE_DATE_RANGE:
                        passed = AssertConstraint.affirm(c, shot.getShotDate());
                        break;
                    /* need some test cases */
                    case OPTIONAL_GIVEN_BY:
                        passed = !(AssertConstraint.affirm(c,
                                report.getScheduleCase().getDob(),
                                er.getPreviousValidShot().getShotDate()));  
                        break;
                    case MINIMUM_AGE_DATE_RANGE:
                        passed = AssertConstraint.affirm(c, report.getScheduleCase().getDob(), shot.getShotDate());
                        break;
                    case MINIMUM_INTERVAL_RANGE:
                        passed = AssertConstraint.affirm(c,
                                 er.getPreviousValidShot().getShotDate(),
                                 shot.getShotDate());
                        break;
                    case MINIMUM_INTERVAL_TO_PRIOR2_RANGE:
                        passed = AssertConstraint.affirm(c,
                                 er.getShotBeforePreviousValidShot().getShotDate(),
                                 shot.getShotDate());
                        break;
                    default:
                }
                if (!passed) {
                    break;
                }
            }
            /* passed all constraint assessment*/
            if (passed || (!passed && name.equals(ConstraintOf.OPTIONAL_GIVEN_BY)) ) {
                //validDoses.add(ds);
                if (er.getValidShot() == null) {
                    er.setValidShot(new ArrayList<>());
                }
                if (!er.getValidShot().contains(shot)){
                    if (passed){
                        er.getValidShot().add(shot);
                    }
                }
                Collection<? extends Dose> sucessor = 
                        SeriesScheduleUtil.getDoses(ds, report.getSeries());
                sucessor = filter.filter(sucessor);
                if(sucessor == null){
                    er.setCompleted(true);
                    break;
                }else {
                    validDoses.addAll(sucessor);
                }
            } 
        }
        return validDoses.isEmpty()? null:validDoses;
    }

    @Override
    public DefaultProcessReport getProcessReport() {
        return report;
    }

    @Override
    public void setProcessReport(DefaultProcessReport report) {
        this.report = report;
    }

    private Shot<LocalDate> getCurrentShot() {
        DefaultEvaluationReport er = (DefaultEvaluationReport) report.getEvaluationReport();
        int index = er.getCurrentShotNumber();
        if (report.getScheduleCase().getShots().size() > index) {
            return (Shot<LocalDate>) ((ArrayList) report.getScheduleCase().getShots()).get(index);
        } else {
            return null;
        }
    }

    @Override
    public void setDoseFilter(DoseFilter filter) {
        this.filter = filter; 
    }
}
