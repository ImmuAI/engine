/*
 *
 * SPDX-FileCopyrightText: 2012-2022 Weimim Wu <wu.weimin.01@gmail.com>
 *
 * SPDX-License-Identifier: MIT OR MulanPSL-2.0 OR Apache-2.0 OR LGPL-3.0-or-later
 *
 */
package org.immunization.process.api.impl;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.immunization.process.api.Transform;
import org.immunization.process.series.api.ProcessReport;
import org.immunization.process.series.api.impl.DefaultRecommendationReport;
import org.immunization.record.api.RecommendationShot;
import org.immunization.record.api.Vaccine;
import org.immunization.record.api.impl.DefaultRecommendationShot;
import org.immunization.record.api.impl.ImmunizationReport;
import org.immunization.schedule.api.RecommendedShot;
import org.immunization.schedule.api.Shot;
import org.immunization.schedule.api.VaccineComponent;
import org.immunization.schedule.api.impl.DefaultRecommendedShot;
import org.immunization.schedule.api.impl.DefaultVaccineComponent;

/**
 *
 * @author Weimin Wu < wu.weimin.01@gmail.com>
 * @version 1.0
 * Last Modified: Jan 9, 2022 5:30:12 PM
 */
class TransformRecommendedShot implements
        Transform<Collection<ProcessReport<Shot<LocalDate>, LocalDate>>, ImmunizationReport> {

    //  private DefaultProcessReport report;
    private DefaultRecommendationReport r;
    private HashMap<Vaccine, Collection<VaccineComponent>> combinedVaccineFinder;
    private String fluSeries = "1501";

    public TransformRecommendedShot() {
        combinedVaccineFinder = new HashMap<>();
    }

    public ImmunizationReport transform(
            Collection<ProcessReport<Shot<LocalDate>, LocalDate>> seriesCaseReports) {
        /* case initilization */
        ImmunizationReport report = new ImmunizationReport();
        //  report.setRecord(icase);
        ArrayList<String> finishedSeries = new ArrayList<>();

        /* for merge combined recommendation shot */
        //ArrayList<RecommendationShot> tempCombinedShots = new ArrayList<>();
        HashMap<String, Collection<RecommendationShot>> seriesRecommendation
                = new HashMap<>();
        HashMap<Vaccine, Collection<RecommendedShot<LocalDate>>> recommendedCombinedComponent = new HashMap<>();

        /* iterate all the series */
        for (ProcessReport<Shot<LocalDate>, LocalDate> pr : seriesCaseReports) {
            if (pr.getEvaluationReport() != null
                    && pr.getEvaluationReport().isCompleted()) {
                /* process completed series*/
                finishedSeries.add(pr.getSeries());
                continue;
            }
            if (pr.getSeries().equals(fluSeries)) {
                continue;
            }
            /* process recommended shots */
            r = (DefaultRecommendationReport) pr.getRecommendationReport();
            Collection<RecommendedShot<LocalDate>> shots = r.getRecommenedShot();
            if (!shots.isEmpty()) {
                for (RecommendedShot<LocalDate> shot : shots) {
                    Collection<? extends RecommendationShot> rshot
                            = createRecommendationShot(shot, recommendedCombinedComponent);

                    /* add to report. */
                    if (rshot != null) {
                        Collection<RecommendationShot> sr = seriesRecommendation.get(pr.getSeries());
                        if (sr == null) {
                            sr = new ArrayList<>();
                            seriesRecommendation.put(pr.getSeries(), sr);
                        }
                        sr.addAll(rshot);
                        Collections.sort((List)sr);
                    }
                }
            }
            // pr.getEvaluationReport().getInvalidShot();
        }
        /* update report with series colected info*/
        if (!finishedSeries.isEmpty()) {
            report.setCompletedSeries(finishedSeries);
        }
        /* process combination shot and update report*/
        if (!recommendedCombinedComponent.isEmpty()) {
            Collection<RecommendationShot> combinedShots
                    = createCombinedRecommendationShot(recommendedCombinedComponent);
            if (combinedShots != null) {
                report.setCombinedVaccineShots(combinedShots);
            }
        }
        /* update report with recommendation shot by series*/
        report.setShots(seriesRecommendation);
        postFilter(report);
        /// report.
        return report;
    }

    private void postFilter(ImmunizationReport report) {
        Collection<RecommendationShot> combinedShots = report.getCombinedVaccineShots();
        if (combinedShots != null) {
            Map<String, Collection<RecommendationShot>> seriesRecommendation
                    = report.getShots();
            report.setCombinedVaccineShotSeries(new ArrayList<String>());
            for (RecommendationShot shot : combinedShots) {
                for (VaccineComponent vc : combinedVaccineFinder.get(shot.getVaccine())) {
                    if (vc instanceof DefaultVaccineComponent) {
                        DefaultVaccineComponent c = (DefaultVaccineComponent) vc;
                        report.getCombinedVaccineShotSeries().add(c.getSeriesCode());
                    }
                }
            }
        }  
    }

    private Collection<? extends RecommendationShot> createRecommendationShot(
            RecommendedShot<LocalDate> shot,
            HashMap<Vaccine, Collection<RecommendedShot<LocalDate>>> recommendedCombinedComponent) {
        Collection<Vaccine> vaccines = getVaccine(shot.getVaccine().getCode());
        ArrayList<DefaultRecommendationShot> shots = new ArrayList<>();

        for (Vaccine v : vaccines) {
            /* find combined vaccine for later process */
            Collection<VaccineComponent> vc = null;
            if (combinedVaccineFinder.containsKey(v)) {
                vc = combinedVaccineFinder.get(v);
            } else {
                vc = getComponent(v);
                combinedVaccineFinder.put(v, vc);
            }
            if (vc.size() > 1) { //one-to-many v-c
                /* update finder for later use */

 /* save combined recommendation shot for later process */
                Collection<RecommendedShot<LocalDate>> components;
                if (recommendedCombinedComponent.containsKey(v)) {
                    components = recommendedCombinedComponent.get(v);
                } else {
                    components = new ArrayList<>();
                    recommendedCombinedComponent.put(v, components);
                }
                components.add(shot);
            } else { // one-on-one v-c
                DefaultRecommendationShot s = new DefaultRecommendationShot();
                s.setVaccine(v);
                s.setShotDate(shot.getShotDate());
                s.setShotDatePeriod(shot.getShotDatePeriod());
                if (shot instanceof DefaultRecommendedShot) {
                    DefaultRecommendedShot dshot = (DefaultRecommendedShot) shot;
                    s.setMinimumShotDate(dshot.getMinimumShotDate());
                    s.setMinimumShotDatePeriod(dshot.getMinimumShotDatePeriod());
                }
                shots.add(s);
            }
        }
        return shots.size() == 0 ? null : shots;
    }

    private Collection<RecommendationShot> createCombinedRecommendationShot(
            HashMap<Vaccine, Collection<RecommendedShot<LocalDate>>> recommendedCombinedComponent) {
        ArrayList<RecommendationShot> shots = new ArrayList<>();
        for (Vaccine v : recommendedCombinedComponent.keySet()) {
            if (combinedVaccineFinder.get(v).size()
                    == recommendedCombinedComponent.get(v).size()) {
                DefaultRecommendationShot s = new DefaultRecommendationShot();
                s.setVaccine(v);
                //need merge time of compoennts
                LocalDate shotDate = null, toshot = null;
                LocalDate mshotDate = null, mtoshot = null;
                Period p = null, mp = null;
                for (RecommendedShot<LocalDate> componentShot
                        : recommendedCombinedComponent.get(v)) {
                    p = componentShot.getShotDatePeriod();
                    if (shotDate == null) {
                        shotDate = componentShot.getShotDate();
                        if (p != null) {
                            toshot = shotDate.plus(p);
                        }
                    } else {
                        shotDate = shotDate.isAfter(componentShot.getShotDate())
                                ? shotDate : componentShot.getShotDate();
                        if (p != null) {
                            LocalDate t = (componentShot.getShotDate()).plus(p);
                            toshot = toshot == null || toshot.isBefore(t) ? t : toshot;
                        }
                    }
                    // s.setRecommendationDateRange(shot.getRecommendedDateRange());
                    if (componentShot instanceof DefaultRecommendedShot) {
                        DefaultRecommendedShot cshot = (DefaultRecommendedShot) componentShot;
                        mp = cshot.getMinimumShotDatePeriod();
                        if (mshotDate == null) {
                            mshotDate = cshot.getMinimumShotDate();
                            if (p != null) {
                                mtoshot = mshotDate.plus(p);
                            }
                        } else {
                            mshotDate = mshotDate.isAfter(cshot.getMinimumShotDate())
                                    ? shotDate : cshot.getMinimumShotDate();
                            if (p != null) {
                                LocalDate t = cshot.getMinimumShotDate().plus(p);
                                mtoshot = mtoshot == null || mtoshot.isBefore(t) ? t : mtoshot;
                            }
                        }
                    }
// s.setMinimumDateRange(shot.getMinimumDateRange());
                }
                s.setShotDate(shotDate);
                s.setShotDatePeriod(p);
                s.setMinimumShotDate(shotDate);
                s.setMinimumShotDatePeriod(p);
                shots.add(s);
            }
        }
        //remove series recommendation?
        return shots.size() == 0 ? null : shots;
    }

    private Collection<Vaccine> getVaccine(String component) {
        return (DefaultResourceService.getInstance()
                .getVaccineManager().getVaccine(component));
    }

    private Collection<VaccineComponent> getComponent(Vaccine vaccine) throws IllegalArgumentException {
        Collection<VaccineComponent> vc = DefaultResourceService.getInstance()
                .getVaccineManager().getVaccineComponent(vaccine.getCode());
        if (vc == null) {
            throw new IllegalArgumentException(vaccine.getName() + ": Vaccine not found.");
        }
        return vc;
    }
}
